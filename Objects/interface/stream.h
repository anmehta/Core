#ifndef _DAS_STREAM_
#define _DAS_STREAM_

#include <iostream>

#include "Core/CommonTools/interface/variables.h"
std::ostream& operator<< (std::ostream& Stream, const DAS::FourVector& p4);

#include "Core/Objects/interface/Lepton.h"
std::ostream& operator<< (std::ostream& Stream, const DAS::GenMuon& muon);
std::ostream& operator<< (std::ostream& Stream, const DAS::RecMuon& muon);

#include "Core/Objects/interface/Jet.h"
std::ostream& operator<< (std::ostream& Stream, const DAS::GenJet& jet);
std::ostream& operator<< (std::ostream& Stream, const DAS::RecJet& jet);

#endif
