#ifndef _Event_
#define _Event_

#include <vector>

#include "Core/CommonTools/interface/variables.h"
#include <TRandom3.h>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// class Event
///
/// Contains:
///  - weights (as a vector, so as to include systematics)
///  - event information (a priori only relevant for data)
///  - hard_scale (only relevant for simulation)
struct Event {
    std::vector<float> genWgts, //!< gen-level event weights (e.g. cross section normalisation)
                       recWgts; //!< rec-level event weights (e.g. PU corrections)
    int runNo, //!< 6-digit run number
        lumi;  //!< lumi section
    unsigned long long evtNo; //!< event number
    float hard_scale; //!< hard scale, corresponding to `pthat` in Pythia 8

    void clear (); //!< to clear for each new event in *n*-tupliser
};

////////////////////////////////////////////////////////////////////////////////
/// class Trigger
///
/// This class only makes sense for data.
/// It mainly contains the bit of the fired triggers.
/// Originally, it was also supposed to contain the prescale information; the effective luminosties are used instead.
struct Trigger {
    std::vector<bool> Bit; //!< indicates which trigger has fired
    std::vector<int> PreHLT,   //!< HLT prescale
                     PreL1min, //!< L1 min pre-scale
                     PreL1max; //!< L1 max pre-scale

    void clear (); //!< to clear for each new event in *n*-tupliser
};

////////////////////////////////////////////////////////////////////////////////
/// class MET
///
/// Intended mostly for checks, but not used explicitely anywhere in the analysis.
struct MET {
    float Et,    //!< transverse energy
          SumEt, //!< sum of the transverse energies of all the components (jet, leptons, etc.) present in the event
          Pt,    //!< exactly the same as Et
          Phi;   //!< direction of the total transverse momentum
    std::vector<bool> Bit; //!< flags for application of MET filters (see *n*-tupliser config file)

    void clear (); //!< to clear for each new event in *n*-tupliser
};

////////////////////////////////////////////////////////////////////////////////
/// class PileUp
///
/// Contains all relevant information to apply the PU staub sauger and profile reweighting.
struct PileUp {

    static float MBxsec, MBxsecRelUnc;

    float rho;      //!< soft activity (see formula 7.15 in Patrick's thesis)
    int nVtx;       //!< number of vertices in the event
    float trpu;     //!< true pile-up
    int intpu;      //!< in-time pile-up (i.e. from the same bunch crossing)
    float pthatMax; //!< hard-scale of the hardest pile-up event (for PUstaubSauger)
    float GetTrPU (const char v = '0') const; //!< pile-up variations
    float GetInTimePU (const char v = '0', const bool useDefault = true) const; //!< pile-up variations    
    static TRandom3 r3;
    void clear (); //!< to clear for each new event in *n*-tupliser
};

////////////////////////////////////////////////////////////////////////////////
/// class PrimaryVertex
///
/// Contains all relevant information on the primary vertex.
struct PrimaryVertex {
    float Rho,  //!< transverse distance to beam axis
          z,    //!< position on beam axis
          chi2, //!< figure of merit of the vertex fit
          ndof; //!< number of degrees of freedom in vertex fit
    bool fake; //!< flag for fake vertices

    void clear (); //!< to clear for each new event in *n*-tupliser
};

////////////////////////////////////////////////////////////////////////////////
/// class SecondaryVertex
///
/// Contains all relevant information on the secondary vertices.
struct SecondaryVertex {

    TriVector position; //!< spatial position of vertex
    float chi2, //!< figure of merit of the vertex fit
          ndof; //!< number of degrees of freedom in vertex fit

    void clear (); //!< to clear for each new event in *n*-tupliser
};

}

#if defined(__ROOTCLING__)
#pragma link C++ class DAS::Event +;

#pragma link C++ class DAS::Trigger +;

#pragma link C++ class DAS::PrimaryVertex +;

#pragma link C++ class DAS::PileUp +;

#pragma link C++ class DAS::MET +;

#endif

#endif
