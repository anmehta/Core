#pragma once

#include <vector>

#include "Core/CommonTools/interface/variables.h"

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// class GenPhoton
///
/// Contains four-momentum
struct GenPhoton {
    FourVector p4; //!< Four-momentum
    bool zAncestor;

    GenPhoton() = default; //!< Constructor (trivial)
};

////////////////////////////////////////////////////////////////////////////////
/// class RecPhoton
///
/// The four-momentum p4 does not include the energy calibration.
struct RecPhoton {
    /// Energy scale and smearing variations provided by EGamma
    /// See https://twiki.cern.ch/twiki/bin/view/CMS/EgammaMiniAODV2#Energy_Scale_and_Smearing rev 19
    /// and https://twiki.cern.ch/twiki/bin/view/CMS/EgammaUL2016To2018#Scale_and_smearing_corrections_f rev 73
    enum EnergyVariation {
        Nominal = 0,    //!< ecalEnergy of photon after scale & smearing corrections
        ScaleUp,        //!< energy with the ecal energy scale shifted 1 sigma(stat) up
        ScaleDown,      //!< energy with the ecal energy scale shifted 1 sigma(stat) down
        SigmaPhiUp,     //!< energy with the ecal energy smearing value shifted 1 sigma(phi) up
        SigmaPhiDown,   //!< energy with the ecal energy smearing value shifted 1 sigma(phi) down
        SigmaRhoUp,     //!< energy with the ecal energy smearing value shifted 1 sigma(rho) up
        SigmaRhoDown,   //!< energy with the ecal energy smearing value shifted 1 sigma(rho) down
        VariationsCount //!< Number of available variations
    };

    /// Identification flags provided by EGamma
    /// See https://twiki.cern.ch/twiki/bin/view/CMS/EgammaRunIIRecommendations#Fall17v2_AN1 rev 20
    /// Values can be combined as a bit field, e.g. `CutBasedLoose | CutBasedMedium`.
    enum Identification {
        CutBasedLoose              = 0b000001, //!< Loose cut-based ID
        CutBasedMedium             = 0b000010, //!< Medium cut-based ID
        CutBasedTight              = 0b000100, //!< Tight cut-based ID
        MVAWorkingPoint80          = 0b001000, //!< 80% efficiency working point of the MVA ID
        MVAWorkingPoint90          = 0b010000, //!< 90% efficiency working point of the MVA ID
        ConversionSafeElectronVeto = 0b100000, //!< Electron veto
    };

    FourVector p4; //!< Four-momentum, without energy corrections
    std::vector<float> weights; //!< Photon weights

    float scEta; //!< Super cluster eta, used to veto the barrel/endcap transition region

    /// \brief Energy scale and smearing variations, indexed with the \ref EnergyVariation enum.
    /// \see \ref CorrP4
    std::array<float, VariationsCount> scales;
    float ecalEnergyErrPostCorr = -1; //!< resolution estimate on the ecalEnergy after scale & smearing corrections

    std::uint32_t selectors = 0; //!< Identification cuts satisfied by the photon

    RecPhoton () = default; //!< Constructor (trivial)

    FourVector CorrP4 (EnergyVariation var = Nominal) const;
};

/// Obtain the calibrated (data) or smeared (MC) 4-momentum for the given variation
/// of the corrections.
inline FourVector RecPhoton::CorrP4 (EnergyVariation var) const
{
    FourVector corrected = p4;
    corrected.Scale(scales[var]);
    return corrected;
}

}

#if defined(__ROOTCLING__)
#pragma link C++ class DAS::GenPhoton +;
#pragma link C++ class std::vector<DAS::GenPhoton> +;

#pragma link C++ class DAS::RecPhoton +;
#pragma link C++ class std::vector<DAS::RecPhoton> +;

#endif
