#ifndef _Leptons_
#define _Leptons_

#include <vector>
#include <iostream>

#include "Core/CommonTools/interface/variables.h"

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// class GenMuon
///
/// Contains four-momentum 
struct GenMuon {

    int Q; //!< +/- 1

    FourVector p4; //!< Four-momentum

    std::vector<float> weights; //!< muon weight 

    GenMuon (); //!< Constructor (trivial)

};

////////////////////////////////////////////////////////////////////////////////
/// class RecMuon
struct RecMuon : public GenMuon {

    unsigned int selectors; //!< [muon ID & PF isolation](https://github.com/cms-sw/cmssw/blob/CMSSW_9_4_X/DataFormats/MuonReco/interface/Muon.h#L188-L212)

    std::vector<float> scales; //!< e.g. Rochester corrections

    //
    RecMuon (); //!< Constructor (trivial)

    float RawPt () const; //!< w/o energy correction
    float CorrPt          //!< w. energy correction
        (size_t i = 0) const; //!< index in scale vector
};

}

#if defined(__ROOTCLING__)
#pragma link C++ class DAS::GenMuon +;
#pragma link C++ class std::vector<DAS::GenMuon> +;

#pragma link C++ class DAS::RecMuon +;
#pragma link C++ class std::vector<DAS::RecMuon> +;

#endif

#endif
