#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>
#include <utility>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"

#include "Core/PUstaubSauger/interface/sigma.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>

#include "Math/VectorUtil.h"

#include <protodarwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::PUstaub {

////////////////////////////////////////////////////////////////////////////////
/// Get plots for PU staub (i.e. high-weight events due to bad PU sampling)
void getPUstaub 
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;
    
    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );

    Event * event = nullptr;
    PileUp * pileup = nullptr;
    pair<bool, float> distGenPV(false, -999); //TODO PrimaryVertex object in the n-tuple TODO Once everyone has rerun ntuples turn this variable to plain float
    tIn->SetBranchAddress("event", &event);
    tIn->SetBranchAddress("pileup", &pileup);
    if (metainfo.Find("flags", "labels", "distGenPV")) { //TODO Remove this once everyone has rerun ntuples
        cout << orange << "Loading distGenPV branch.." << def << endl;
        distGenPV.first = true;
        tIn->SetBranchAddress("distGenPV", &distGenPV.second);
    }

    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    tIn->SetBranchAddress("recJets", &recjets);
    tIn->SetBranchAddress("genJets", &genjets);

    auto PUcleaning = config.get<fs::path>("corrections.PUstaub.PUcleaning");
    PUstaub::Sauge sauge(PUcleaning);
    metainfo.Set<fs::path>("corrections", "PUstaub", "PUcleaning", PUcleaning);


    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        sauge(pileup, event, recjets, genjets, distGenPV);
    }
    
    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();
    sauge.Write(fOut.get());

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::PUstaub namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("`getPUstaub` is an intermediate processing step to later obtain the cut-off\n"
                            "correction file required in `applyMClumi`.\n"
                            "The <PUcleaning> argument is just required due to the structure of the sigma.h library.\n"
                            "So even a dummy file can be provided at this stage, e.g.,\n\n"
                            "\x1B[33mptmin  maxlogw\n"
                            "1 999.0\x1B[30m\e[0m\n\n"
                            "`getPUstaub` gets the leading jet pt as a function of event weight from the n-tuples, "
                            "to determine the overweighted events due to the PU sampling.",
                            DT::config | DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("PUcleaning", "corrections.PUstaub.PUcleaning", "2-column file with limits (location `$DARWIN_TABLES/PUstaub/ak?`)");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::PUstaub::getPUstaub(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
