#include <iostream>
#include <vector>
#include <filesystem>
#include <utility>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include "Core/PUstaubSauger/interface/sigma.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>

#include <protodarwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::PUstaub {

////////////////////////////////////////////////////////////////////////////////
/// Remove PU staub (i.e. high-weight events due to bad PU sampling)
void applyPUstaubSauger 
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;
    
    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );
    if (!metainfo.Find("corrections", "xsection"))
        BOOST_THROW_EXCEPTION( DE::BadInput("The input should be normalised before running the PU cleaning.",
                                      make_unique<TFile>(inputs.front().c_str() )) );

    // removal / staub sauger
    auto MEscale = config.get<fs::path>("corrections.PUstaub.MEscale");
    metainfo.Set<fs::path>("corrections", "PUstaub", "MEscale", MEscale);
    auto rescale =  DT::GetHist<TH1>({MEscale}, "dirty/pthat");
    rescale->Divide(DT::GetHist<TH1>({MEscale}, "blink/pthat").get()); // TODO: handle directories a la inputs

    auto PUcleaning = config.get<fs::path>("corrections.PUstaub.PUcleaning");
    PUstaub::Sauge sauge(PUcleaning);
    if (metainfo.Get<fs::path>("corrections", "PUstaub", "PUcleaning") != PUcleaning)
        metainfo.Set<fs::path>("corrections", "PUstaub", "PUcleaning", PUcleaning);
    PUstaub::Plots polish("polish", "after hard scale correction (final)");

    // branches
    Event * event = nullptr;
    PileUp * pileup = nullptr;
    pair<bool, float> distGenPV(false, -999); //TODO PrimaryVertex object in the n-tuple
    tIn->SetBranchAddress("event", &event);
    tIn->SetBranchAddress("pileup", &pileup);
    if (metainfo.Find("flags", "labels", "distGenPV")) { //TODO Remove this once everyone has rerun n-tuples
        cout << orange << "Loading distGenPV branch.." << def << endl;
        distGenPV.first = true;
        tIn->SetBranchAddress("distGenPV", &distGenPV.second);
    }
    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    tIn->SetBranchAddress("recJets", &recjets);
    tIn->SetBranchAddress("genJets", &genjets);

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw"),
                 corrected("corrected");

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        raw(*genjets, event->genWgts.front());
        raw(*recjets, event->genWgts.front() * event->recWgts.front());

        if (sauge(pileup, event, recjets, genjets, distGenPV)) 
            continue;

        int iscale = rescale->FindBin(event->hard_scale);
        auto correction = rescale->GetBinContent(iscale);
        // I am commenting out the rescale
        for (auto& w: event->genWgts)
            w *= correction;

        corrected(*genjets, event->genWgts.front());
        corrected(*recjets, event->genWgts.front() * event->recWgts.front());

        polish.Fill(pileup, event, recjets, genjets, distGenPV);

        if ((steering & DT::fill) == DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();
    sauge.Write(fOut.get());
    raw.Write(fOut.get());
    polish.Write(fOut.get());
    corrected.Write(fOut.get());

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::PUstaub namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Cut off bad events in MC samples.", DT::config | DT::split | DT::fill);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("PUcleaning", "corrections.PUstaub.PUcleaning", "2-column file with limits (location `$DARWIN_TABLES/PUstaub/ak?`)")
               .arg<fs::path>("MEscale", "corrections.PUstaub.MEscale", "output from getPUstaub or applyMClumi");
                

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::PUstaub::applyPUstaubSauger(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
