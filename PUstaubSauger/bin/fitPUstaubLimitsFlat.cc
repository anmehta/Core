#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>
#include <memory>

#include "Core/CommonTools/interface/variables.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <TString.h>
#include <TFile.h>
#include <TH2.h>
#include <TF1.h>

#include <protodarwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::PUstaub {

////////////////////////////////////////////////////////////////////////////////
/// Exclude PU contribution at high pt by performing a double Gaussian fit.
/// This logic was set up for the SMP-20-011 analysis, and should be revisited
/// for other samples.
/// 
/// Output is similar to that of `getPUstaubLimitsSlices` with a max weight per rec pt bin.
void fitPUstaubLimitsFlat
        (const vector<fs::path>& inputs,
         const fs::path& outputRoot,
         const fs::path& outputTxt,
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering //!< parameters obtained from explicit options
        )
{
    cout << __func__ << " start" << endl;

    auto h2 = DT::GetHist<TH2>(inputs, "clean/recptLogWgt");

    auto f = make_unique<TFile>(outputRoot.c_str(), "RECREATE");

    pt::ptree table;
    int N = h2->GetNbinsX();
    cout << "ptmin\tmaxlogw" << endl;
    for (int i=1; i<=N; ++i) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        int ptmin = h2->GetXaxis()->GetBinLowEdge(i);

        TH1 * p = h2->ProjectionY(Form("p%d",ptmin),i,i);
        float a = p->Integral();
        static const auto feps = numeric_limits<float>::epsilon();
        if (a < feps) continue;
        p->Scale(1./a);
        p->Write();

        // fit
        // 1) simple gaus
        p->Fit("gaus", "0Q");
        TF1 *f = p->GetFunction("gaus");
        double par[3];
        f->GetParameters(par);
        
        // 2) double gaus
        auto doublegaus = make_unique<TF1>(Form("f%d", ptmin), "gaus(0)", -30, 20);
        doublegaus->SetParameters(par);
        doublegaus->SetLineWidth(1);
        p->Fit(doublegaus.get(), "0Q");
        doublegaus->Write();
        doublegaus->GetParameters(par);
        float maxlogw = par[1] + 4 * par[2]; // we exclude anything at more than four sigmas from the

        // write
        cout << ptmin << ' ' << maxlogw << endl;
        table.put<float>(to_string(ptmin), maxlogw);
    }

    pt::write_info(outputTxt.string(), table);

    cout << __func__ << " stop" << endl;
}

} // end of DAS::PUstaub namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path outputRoot, outputTxt;

        DT::Options options("Determines the maximum weight in jet high-pileup samples.\n"
                            "NB: this executable expect a flat sample!");
        options.inputs("inputs", &inputs, "input ROOT files (from `getPUstaub`)")
               .output("outputRoot", &outputRoot, "output ROOT file with histograms")
               .output("outputTxt" , &outputTxt , "output 2-column file with limits");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::PUstaub::fitPUstaubLimitsFlat(inputs, outputRoot, outputTxt, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
