#include <cmath>
#include <filesystem>
#include <utility>

#include <TFile.h>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/PUstaubSauger/interface/sigma.h"

#include "Core/Objects/interface/Event.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

using namespace std;
namespace fs = std::filesystem;

namespace DAS::PUstaub {

static const vector<float> distGenPV_edges {0, 0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16, 0.18, 0.2, 0.22, 0.24, 0.26, 0.28, 0.3, 0.32, 0.34, 0.36, 0.38, 0.4, 0.42, 0.44, 0.46, 0.48, 0.5, 0.52, 0.54, 0.56, 0.58, 0.6, 0.62, 0.64, 0.66, 0.68, 0.7, 0.72, 0.74, 0.76, 0.78, 0.8, 0.82, 0.84, 0.86, 0.88, 0.9, 0.92, 0.94, 0.96, 0.98, 1, 1.48, 1.96, 2.44, 2.92, 3.4, 3.88, 4.36, 4.84, 5.32, 5.8, 6.28, 6.76, 7.24, 7.72, 8.2, 8.68, 9.16, 9.64, 10.12, 10.6, 11.08, 11.56, 12.04, 12.52, 13, 13.48, 13.96, 14.44, 14.92, 15.4, 15.88, 16.36, 16.84, 17.32, 17.8, 18.28, 18.76, 19.24, 19.72, 20.2, 20.68, 21.16, 21.64, 22.12, 22.6, 23.08, 23.56, 24.04, 24.52, 25};

static const int distGenPVBins = distGenPV_edges.size()-1;

static const float distGenPVmin = distGenPV_edges.front(),
                   distGenPVmax = distGenPV_edges.back();

Sauge::Sauge (std::filesystem::path p) :
    dist (                   new Plots("dist" , "PV distance only cut-off")),
    scale(                   new Plots("scale", "hard scale only cut-off")),
    dirty(                   new Plots("dirty", "raw content without any cut")),
    clean(                   new Plots("clean", "PV distance & hard scale cut-offs")),
    blink(p != "/dev/null" ? new Plots("blink", "PV distance, hard scale & max weight cut-offs") : nullptr)
{
    // TODO: use Boost Property Trees...
    cout << p << endl;
    assert(fs::exists(p)); // TODO

    ifstream file(p.c_str());

    {
        TString ptmin, maxlogw;
        file >> ptmin >> maxlogw;
    }
    
    while (file.good()) {

        // extracting
        double ptmin, maxlogw;
        file >> ptmin >> maxlogw;
        cout << ptmin << ' ' << maxlogw << '\n'; // TODO: improve verbosity
        if (cuts.find(ptmin) == cuts.end())
            cuts.insert({ptmin, maxlogw});
    }
    cout << flush;
    file.close();
}

void Sauge::Write (TDirectory * dir) const
{
    for (auto plot: {dist, scale, dirty, clean, blink}) {
        if (plot == nullptr) continue;
        dir->cd();
        plot->Write(dir);
    }
}

bool Sauge::operator()
        (PileUp * pileup,
         Event * evnt,
         vector<RecJet> * recjets,
         vector<GenJet> * genjets,
         pair<bool, float> distGenPV) //TODO: add to PrimaryVertex structure
{
    dirty->Fill(pileup, evnt, recjets, genjets, distGenPV); 

    // additional cut, based on the distance PV between gen and rec level
    // requiring  |distGenPV|<1 cm // tuned to data
    if (distGenPV.first && distGenPV.second <= 1) //TODO Remove `distGenPV.first == true` once everyone has rerun ntuples
        dist->Fill(pileup, evnt, recjets, genjets, distGenPV);

    // first cut, based on hard scale
    double PU_hard_scale = pileup->pthatMax,
           ME_hard_scale = evnt->hard_scale;
    if (PU_hard_scale <= ME_hard_scale)
        scale->Fill(pileup, evnt, recjets, genjets, distGenPV);

    // Checking both cuts together and fill single histo after both conditions
    //
    
    if ((distGenPV.first && distGenPV.second > 1) || PU_hard_scale > ME_hard_scale) return true; // bad event TODO Remove `distGenPV.first == true` once everyone has rerun ntuples
    clean->Fill(pileup, evnt, recjets, genjets, distGenPV);

    // last cut, based on weight and leading pt
    bool Bad = false;
    if (recjets->size() > 0) {
        int pt = recjets->front().p4.Pt();
        double w = evnt->genWgts.front();
        for (auto it = cuts.rbegin(); it != cuts.rend(); ++it) {
            if (pt < it->first) continue;
            Bad = log(w) > it->second;
            break;
        }
    }
    if (!Bad) blink->Fill(pileup, evnt, recjets, genjets, distGenPV);
    return Bad;
}

TH1 * Plots::MakeHist (TString name, TString title)
{
    return new TH1D(name + title, title, nPtBins, pt_edges.data());
}

TH2 * Plots::MakeHist2D (TString name, TString title)
{
    return new TH2D(name + title, title, nPtBins, pt_edges.data(), nYbins, y_edges.data());
}

Plots::Plots (TString Name, TString Title) :
    name(Name), title(Title),
    genpt     (MakeHist(name, "genpt"     )),
    recpt     (MakeHist(name, "recpt"     )),
    pthat     (MakeHist(name, "pthat"     )),
    pthatMax  (MakeHist(name, "pthatMax"  )),
    pthatMaxPU(MakeHist(name, "pthatMaxPU")),
    distGenRecPV (new TH1F(name + "distGenRecPV", "distGenRecPV", distGenPVBins, distGenPV_edges.data())),
    intprofile (new TH1D("intPU", ";in-time PU;N^{ev}_{eff}", nPUbins, 0, nPUbins)),
    genpt_y   (MakeHist2D(name, "genpt_y" )),
    recpt_y   (MakeHist2D(name, "recpt_y" )),
    recptLogWgt  (new TH2D(name + "recptLogWgt", "recptLogWgt", nPtBins, pt_edges.data(), 400, -30, 20)),
    pthat_intPU  (new TH2D("pthat_intPU", "pthat_intPU", nPtBins, pt_edges.data(), nPUbins, 0, nPUbins)),
    pthatMax_intPU  (new TH2D("pthatMax_intPU", "pthatMax_intPU", nPtBins, pt_edges.data(), nPUbins, 0, nPUbins)),
    pthatMaxPU_intPU  (new TH2D("pthatMaxPU_intPU", "pthatMaxPU_intPU", nPtBins, pt_edges.data(), nPUbins, 0, nPUbins))
{ }

void Plots::Fill (PileUp * pileup, Event * event, vector<RecJet> * recjets, vector<GenJet> * genjets, pair<bool, float> distGenPV)
{
    auto PU_hard_scale = pileup->pthatMax,
         ME_hard_scale = event->hard_scale,
         gW = event->genWgts.front(),
         rW = event->recWgts.front();
    auto intpu = pileup->intpu;

    for (auto& genjet: *genjets) {
        genpt->Fill(genjet.p4.Pt(), gW);
        genpt_y->Fill(genjet.p4.Pt(), genjet.AbsRap(), gW);
    }
    for (auto& recjet: *recjets) {
        recpt->Fill(recjet.RawPt(), gW * rW);
        recpt_y->Fill(recjet.RawPt(), recjet.AbsRap(), gW * rW);
    }
    if (recjets->size() > 0)
        recptLogWgt->Fill(recjets->front().RawPt(), log(gW * rW)); // we *don't* apply the weight to the entry
    pthat   ->Fill(    ME_hard_scale               , gW);
    pthatMax->Fill(max(ME_hard_scale,PU_hard_scale), gW);
    if (distGenPV.first == true) //TODO Remove `distGenPV.first == true` once everyone has rerun ntuples
        distGenRecPV->Fill(distGenPV.second, gW * rW);
    intprofile->Fill(intpu, gW * rW);
    pthat_intPU->Fill(ME_hard_scale, intpu, gW * rW);
    pthatMax_intPU->Fill(max(ME_hard_scale,PU_hard_scale), intpu, gW * rW);
    pthatMaxPU_intPU->Fill(PU_hard_scale, intpu, gW * rW);
}

void Plots::Write (TDirectory * d) const
{
    TDirectory * dd = d->mkdir(name, title);
    dd->cd();
    for (TH1 * h: {genpt, recpt, pthat, pthatMax, pthatMaxPU, distGenRecPV, intprofile, 
            dynamic_cast<TH1*>(genpt_y), dynamic_cast<TH1*>(recpt_y),
            dynamic_cast<TH1*>(recptLogWgt), dynamic_cast<TH1*>(pthat_intPU), dynamic_cast<TH1*>(pthatMax_intPU), dynamic_cast<TH1*>(pthatMaxPU_intPU)}) {
        h->SetDirectory(dd);
        h->Write(h->GetTitle());
    }
    d->cd();
}

}

