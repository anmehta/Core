#include <vector>
#include <map>
#include <filesystem>
#include <utility>

#include "Core/Objects/interface/Jet.h"

#include <TH2.h>

class TDirectory;

namespace DAS {

class PileUp;
class Event;

namespace PUstaub {

////////////////////////////////////////////////////////////////////////////////
/// structure to host relevant distributions for PU staub removal
struct Plots {

    static TH1 * MakeHist (TString name, TString title);
    static TH2 * MakeHist2D (TString name, TString title);

    const TString name,
                  title;

    TH1 * genpt, * recpt, * pthat, * pthatMax, * pthatMaxPU, * distGenRecPV, * intprofile;
    TH2 * genpt_y, * recpt_y, * recptLogWgt, * pthat_intPU, * pthatMax_intPU, * pthatMaxPU_intPU;

    Plots (TString Name, TString Title);

    void Fill (PileUp * pileup,
               Event * event,
               std::vector<RecJet> * recjets,
               std::vector<GenJet> * genjets,
               std::pair<bool, float> distGenPV);

    void Write (TDirectory *) const;
};

////////////////////////////////////////////////////////////////////////////////
/// functor to apply PU cleaning
struct Sauge {

    std::map<int, float> cuts;

    Plots * dist, * scale, * dirty, * clean, * blink;

    Sauge (std::filesystem::path = "/dev/null");

    ////////////////////////////////////////////////////////////////////////////////
    /// Cutting function for PU staub sauging
    ///
    /// Returns true in case of bad event
    bool operator() 
            (PileUp * pileup,       //!< pile-up, contains hard scale of PU interactions
             Event * event,         //!< event, contains hard scale of ME & event weight
             std::vector<RecJet> * recjets,  //!< collection of reconstructed jets
             std::vector<GenJet> * genjets, //!< collection of reconstructed jets
             std::pair<bool, float> distGenPV); //!< distance between rec and gen PV; TODO: change to PrimaryVertex structure

    void Write (TDirectory *) const;
};

} // end of PUstaub namespace

} // end of DAS namespace
