#ifndef __HELPER__
#define __HELPER__

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"
#include "Core/Objects/interface/Event.h"

// rec jet
#include "DataFormats/PatCandidates/interface/Jet.h"

// gen jet
#include "DataFormats/JetMatching/interface/JetFlavourInfo.h"
#include "DataFormats/JetMatching/interface/JetFlavourInfoMatching.h"

// muons
#include "DataFormats/PatCandidates/interface/Muon.h"

// photons
#include "DataFormats/PatCandidates/interface/Photon.h"

#include "Parameters.h"

namespace DAS {

struct Helper {

    DAS::Parameters& p;

    Helper (DAS::Parameters& parameters);

    DAS::GenJet GetGenJet (const reco::JetFlavourInfoMatching &ijet);
    DAS::GenJet GetGenJet (const reco::Jet &ijet);
    DAS::RecJet GetRecJet (const pat::Jet &ijet);

    bool LooseID (const pat::Jet &jet);
    bool TightID (const pat::Jet &jet);
    bool TightLepVetoID (const pat::Jet &jet);

    DAS::GenMuon GetGenMu (const reco::Candidate &mu);
    DAS::RecMuon GetRecMu (const pat::Muon &mu);

    DAS::GenPhoton GetGenPhoton(const reco::GenParticle &photon, bool zAncestor);
    DAS::RecPhoton GetRecPhoton(const pat::Photon &photon);
};

} // end of DAS namespace

#endif
