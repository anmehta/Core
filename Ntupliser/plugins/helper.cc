#include "helper.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"

#include <iostream>
#include <limits>

using namespace std;

////////////////////////////////////////////////////////////////////////////////
/// Constructor
///
/// Only giving parameters in reference
DAS::Helper::Helper (DAS::Parameters& parameters) : p(parameters) {}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::GenJet` from MiniAOD without flavour
DAS::GenJet DAS::Helper::GetGenJet (const reco::Jet &ijet)
{
    DAS::GenJet jjet;

    // kinematics
    jjet.p4 = DAS::FourVector(ijet.p4());

    return jjet;
}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::GenJet` from MiniAOD with flavour
DAS::GenJet DAS::Helper::GetGenJet (const reco::JetFlavourInfoMatching & ijet)
{
    const reco::Jet& Jet = *(ijet.first.get());
    const reco::JetFlavourInfo& Info = ijet.second;

    DAS::GenJet jjet = GetGenJet(Jet);

    // parton flavour
    jjet.partonFlavour = Info.getPartonFlavour();

    // heavy-flavour hadrons
    jjet.nBHadrons = Info.getbHadrons().size();
    jjet.nCHadrons = Info.getcHadrons().size();

    return jjet;
}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::RecJet` from MiniAOD
///
/// [General instructions](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideBtagValidation)
/// [DeepJet](https://twiki.cern.ch/twiki/bin/view/CMS/DeepJet#94X_installation_recipe_X_10)
DAS::RecJet DAS::Helper::GetRecJet (const pat::Jet &ijet) 
{
    DAS::RecJet jjet;

    // kinematics
    jjet.p4      = ijet.correctedP4("Uncorrected");

    // JEC
    jjet.area    = ijet.jetArea();

    if (p.flavour) {
        // parton flavour
        jjet.partonFlavour = ijet.partonFlavour();

        // heavy-flavour hadrons
        jjet.nBHadrons = ijet.jetFlavourInfo().getbHadrons().size();
        jjet.nCHadrons = ijet.jetFlavourInfo().getcHadrons().size();

        // heavy-flavour tagging
        jjet.DeepJet.probb    = ijet.bDiscriminator("pfDeepFlavourJetTags:probb");
        jjet.DeepJet.probbb   = ijet.bDiscriminator("pfDeepFlavourJetTags:probbb"); 
        jjet.DeepJet.problepb = ijet.bDiscriminator("pfDeepFlavourJetTags:problepb"); 
        jjet.DeepJet.probc    = ijet.bDiscriminator("pfDeepFlavourJetTags:probc");
        //jjet.DeepJet.probcc   = ijet.bDiscriminator("pfDeepFlavourJetTags:probcc"); 
        jjet.DeepJet.probuds  = ijet.bDiscriminator("pfDeepFlavourJetTags:probuds"); 
        jjet.DeepJet.probg    = ijet.bDiscriminator("pfDeepFlavourJetTags:probg"); 

        static const auto eps = 10*numeric_limits<float>::epsilon();
        if (abs(jjet.DeepJet.probb + jjet.DeepJet.probbb + jjet.DeepJet.problepb + jjet.DeepJet.probc + /*jjet.DeepJet.probcc +*/ jjet.DeepJet.probuds + jjet.DeepJet.probg - 1) > eps)
            cerr << abs(jjet.DeepJet.probb + jjet.DeepJet.probbb + jjet.DeepJet.problepb + jjet.DeepJet.probc + /*jjet.DeepJet.probcc +*/ jjet.DeepJet.probuds + jjet.DeepJet.probg - 1) << ' '  << eps << endl;
    }

    jjet.JECs.resize(1, 1.0);

    return jjet;
}

////////////////////////////////////////////////////////////////////////////////
/// Testing loose ID definition for jets (official from JetMET)
///
/// see https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetID
///
/// /!\ these values are intended for 2016 only!!
bool DAS::Helper::LooseID (const pat::Jet &jet)
{
    assert(p.year == 2016); // LooseID is only defined for 2016

    const auto abseta = abs(jet.eta());

    auto NHF = jet.neutralHadronEnergyFraction();
    auto NEMF = jet.photonEnergyFraction();
    auto NumConst = jet.neutralMultiplicity() + jet.chargedMultiplicity();

    auto CHF = jet.chargedHadronEnergyFraction();
    auto CHM = jet.chargedHadronMultiplicity();
    auto CEMF = jet.chargedEmEnergyFraction();

    if (abseta <= 2.7)
        return (NHF<0.99 && NEMF<0.99 && NumConst>1)
            && ((abseta<=2.4 && CHF>0 && CHM>0 && CEMF<0.99) || abseta>2.4);

    auto NumNeutralParticle = jet.neutralMultiplicity();

    if (abseta <= 3.0)
        return NHF<0.98 && NEMF>0.01 && NumNeutralParticle>2;
    
    return NEMF<0.90 && NumNeutralParticle>10;
}

////////////////////////////////////////////////////////////////////////////////
/// Testing tight ID definition for jets (official from JetMET)
///
/// see https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetID
bool DAS::Helper::TightID (const pat::Jet &jet)
{
    const auto abseta = abs(jet.eta());

    auto NHF = jet.neutralHadronEnergyFraction();
    auto NEMF = jet.photonEnergyFraction();
    auto NumConst = jet.neutralMultiplicity() + jet.chargedMultiplicity();

    auto CHF = jet.chargedHadronEnergyFraction();
    auto CHM = jet.chargedHadronMultiplicity();
    auto CEMF = jet.chargedEmEnergyFraction();

    if (abseta <= 2.7)
        switch (p.year) {
            case 2016:
                return (NHF<0.90 && NEMF<0.90 && NumConst>1)
                    && ((abseta<=2.4 && CHF>0 && CHM>0 && CEMF<0.99) || abseta>2.4);
            case 2017:
                return (NHF<0.90 && NEMF<0.90 && NumConst>1)
                    && ((abseta<=2.4 && CHF>0 && CHM>0             ) || abseta>2.4);
            case 2018:
                return (abseta<=2.6 && CHM>0 && CHF>0 && NumConst>1 && NEMF<0.9 && NHF < 0.9 )
                                   || (CHM>0 && NEMF<0.99 && NHF < 0.9);
            default:
                cerr << "Only 2016, 2017 or 2018 is possible\n";
                exit(EXIT_FAILURE);
        }

    auto NumNeutralParticle = jet.neutralMultiplicity();

    if (abseta <= 3.0)
        switch (p.year) {
            case 2016:
                return NHF<0.98 && NEMF>0.01 && NumNeutralParticle>2;
            case 2017:
            case 2018:
                return NEMF>0.02 && NEMF<0.99 && NumNeutralParticle>2;
            default:
                cerr << "Only 2016, 2017 or 2018 is possible\n";
                exit(EXIT_FAILURE);
        }
    
    switch (p.year) {
        case 2016:
            return NEMF<0.90 && NumNeutralParticle>10;
        case 2017:
            return NEMF<0.90 && NHF>0.02 && NumNeutralParticle>10;
        case 2018:
            return NEMF<0.90 && NHF>0.2 && NumNeutralParticle>10;
        default:
            cerr << "Only 2016, 2017 or 2018 is possible\n";
            exit(EXIT_FAILURE);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Testing tight ID definition for jets (official from JetMET)
///
/// see https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetID
bool DAS::Helper::TightLepVetoID (const pat::Jet &jet)
{
    const auto abseta = abs(jet.eta());

    auto NHF = jet.neutralHadronEnergyFraction();
    auto NEMF = jet.photonEnergyFraction();
    auto NumConst = jet.neutralMultiplicity() + jet.chargedMultiplicity();

    auto CHF = jet.chargedHadronEnergyFraction();
    auto CHM = jet.chargedHadronMultiplicity();
    auto CEMF = jet.chargedEmEnergyFraction();
    auto MUF  = jet.muonEnergyFraction();

    if (abseta <= 2.7)
        switch (p.year) {
            case 2016:
                return (NHF<0.90 && NEMF<0.90 && NumConst>1 && MUF<0.80)
                    && ((abseta<=2.4 && CHF>0 && CHM>0 && CEMF<0.90) || abseta>2.4);
            case 2017:
                return (NHF<0.90 && NEMF<0.90 && NumConst>1 && MUF<0.80)
                    && ((abseta<=2.4 && CHF>0 && CHM>0 && CEMF<0.80) || abseta>2.4);
            case 2018:
                return (abseta<=2.6 && MUF<0.80 && CEMF<0.80 && CHM>0 && NHF<0.9 && NEMF<0.90 && CHF>0 && NumConst>1)
                                   || (MUF<0.80 && CEMF<0.80 && CHM>0 && NHF<0.9 && NEMF<0.99);
            default:
                cerr << "Only 2016, 2017 or 2018 is possible\n";
                exit(EXIT_FAILURE);
        }

    auto NumNeutralParticle = jet.neutralMultiplicity();

    if (abseta <= 3.0)
        switch (p.year) {
            case 2016:
                return NHF<0.98 && NEMF>0.01 && NumNeutralParticle>2;
            case 2017:
            case 2018:
                return NEMF>0.02 && NEMF<0.99 && NumNeutralParticle>2;
            default:
                cerr << "Only 2016, 2017 or 2018 is possible\n";
                exit(EXIT_FAILURE);
        }
    
    switch (p.year) {
        case 2016:
            return NEMF<0.90 && NumNeutralParticle>10;
        case 2017:
            return NEMF<0.90 && NHF>0.02 && NumNeutralParticle>10;
        case 2018:
            return NEMF<0.90 && NHF>0.2 && NumNeutralParticle>10;
        default:
            cerr << "Only 2016, 2017 or 2018 is possible\n";
            exit(EXIT_FAILURE);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::GenMuon` from MiniAOD
DAS::GenMuon DAS::Helper::GetGenMu (const reco::Candidate &mu)
{
    DAS::GenMuon Mu;

    // kinematics
    Mu.p4 = mu.p4();

    Mu.Q = mu.charge();

    return Mu;
}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::RecMuon` from MiniAOD
DAS::RecMuon DAS::Helper::GetRecMu (const pat::Muon &mu) 
{
    DAS::RecMuon Mu;

    // kinematics
    Mu.p4 = mu.p4();

    Mu.selectors = mu.selectors();
    Mu.Q = mu.charge();

    return Mu;
}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::GenPhoton` from MiniAOD
DAS::GenPhoton DAS::Helper::GetGenPhoton (const reco::GenParticle &photon, bool zAncestor)
{
    DAS::GenPhoton dasPhoton;

    // kinematics
    dasPhoton.p4 = photon.p4();
    dasPhoton.zAncestor = zAncestor;

    return dasPhoton;
}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::RecLep` from MiniAOD
DAS::RecPhoton DAS::Helper::GetRecPhoton (const pat::Photon &photon)
{
    DAS::RecPhoton dasPhoton;

    // kinematics
    dasPhoton.p4 = photon.p4();
    dasPhoton.scEta = photon.superCluster()->eta();

    const auto raw_energy = dasPhoton.p4.E();
    dasPhoton.scales = decltype(dasPhoton.scales){{
        photon.userFloat("ecalEnergyPostCorr") / raw_energy,
        photon.userFloat("energyScaleUp")      / raw_energy,
        photon.userFloat("energyScaleDown")    / raw_energy,
        photon.userFloat("energySigmaPhiUp")   / raw_energy,
        photon.userFloat("energySigmaPhiDown") / raw_energy,
        photon.userFloat("energySigmaRhoUp")   / raw_energy,
        photon.userFloat("energySigmaRhoDown") / raw_energy,
    }};
    dasPhoton.ecalEnergyErrPostCorr = photon.userFloat("ecalEnergyErrPostCorr");

    // ID
    if (photon.photonID("cutBasedPhotonID-Fall17-94X-V2-loose"))
        dasPhoton.selectors |= DAS::RecPhoton::CutBasedLoose;
    if (photon.photonID("cutBasedPhotonID-Fall17-94X-V2-medium"))
        dasPhoton.selectors |= DAS::RecPhoton::CutBasedMedium;
    if (photon.photonID("cutBasedPhotonID-Fall17-94X-V2-tight"))
        dasPhoton.selectors |= DAS::RecPhoton::CutBasedTight;
    if (photon.photonID("mvaPhoID-RunIIFall17-v2-wp80"))
        dasPhoton.selectors |= DAS::RecPhoton::MVAWorkingPoint80;
    if (photon.photonID("mvaPhoID-RunIIFall17-v2-wp90"))
        dasPhoton.selectors |= DAS::RecPhoton::MVAWorkingPoint90;
    if (photon.passElectronVeto())
        dasPhoton.selectors |= DAS::RecPhoton::ConversionSafeElectronVeto;

    return dasPhoton;
}

