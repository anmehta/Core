#include <string>
#include <cmath>
#include <functional>
#include <cassert>

#include "Core/Ntupliser/plugins/Ntupliser.h"
#include "Core/Ntupliser/plugins/helper.h"

#include "FWCore/Framework/interface/MakerMacros.h"

using namespace std;
using namespace reco;
using namespace pat;
using namespace edm;
// Note: don't use namespace DAS in this file

// sort by decreasing pt
template<typename DAStype> void Sort (vector<DAStype>*& collection)
{
    sort(collection->begin(), collection->end(),
            [](DAStype &a, DAStype &b) { return a.p4.Pt() > b.p4.Pt(); } );
}
template<> void Sort (vector<DAS::FourVector>*& collection)
{
    sort(collection->begin(), collection->end(),
            [](DAS::FourVector &a, DAS::FourVector &b) { return a.Pt() > b.Pt(); } );
}

////////////////////////////////////////////////////////////////////////////////
/// Constructor, only initialising the members.
Ntupliser::Ntupliser (ParameterSet const& cfg) : 
    p(cfg, consumesCollector()), h(p),
    tree(nullptr),
    // jets
    recJets_(           new vector<DAS::RecJet>              ),
    HLTjets_(!p.isMC_ ? new vector<DAS::FourVector> : nullptr),
    genJets_( p.isMC_ ? new vector<DAS::GenJet>     : nullptr),
    // muons
    recMuons_(           p.muons ? new vector<DAS::RecMuon> : nullptr),
    genMuons_(p.isMC_ && p.muons ? new vector<DAS::GenMuon> : nullptr),
    // photons
    recPhotons_(           p.photons ? new vector<DAS::RecPhoton> : nullptr),
    genPhotons_(p.isMC_ && p.photons ? new vector<DAS::GenPhoton> : nullptr),
    // event variables
    jetTrigger_    (p.triggers ? new DAS::Trigger : nullptr),
    muonTrigger_   (p.triggers ? new DAS::Trigger : nullptr),
    event_         (new DAS::Event        ),
    met_           (new DAS::MET          ),
    pileup_        (new DAS::PileUp       ),
    primaryvertex_ (new DAS::PrimaryVertex),
    distGenPV(-1) // TODO: move this in `PrimaryVertex`
{
    cout << __FILE__ << ':' << __func__ << endl;
    recJets_->reserve(25); // rough estimation of number of jets in an event
    met_->Bit.reserve(10); // rough estimation of number of filters
    event_->recWgts.reserve(1);
    if (p.triggers) {
        HLTjets_->reserve(25);
        jetTrigger_->Bit.reserve(15); // rough estimation of number of triggers
        jetTrigger_->PreHLT.reserve(15);
        jetTrigger_->PreL1min.reserve(15);
        jetTrigger_->PreL1max.reserve(15);

        muonTrigger_->Bit.reserve(10);
        muonTrigger_->PreHLT.reserve(10);
        muonTrigger_->PreL1min.reserve(10);
        muonTrigger_->PreL1max.reserve(10);
    }
    if (genJets_) {
        genJets_->reserve(25);
#ifdef PS_WEIGHTS
        event_->genWgts.reserve(150);
#endif
    }
    if (p.muons) {
        recMuons_->reserve(25);
        if (genMuons_)
            genMuons_->reserve(25);
    }
    if (p.photons) {
        recPhotons_->reserve(25);
        if (genPhotons_)
            genPhotons_->reserve(25);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Finds the generated muons, and applies some hard cuts on the phase space.
void Ntupliser::getGenMuons ()
{
    for (const reco::Candidate &lepton: *genLeptons) {
        if (abs(lepton.pdgId()) != 13) continue;

        // we convert the complicated CMSSW object to our simple DAS object
        DAS::GenMuon genmu = h.GetGenMu(lepton);

        // we keep only get with pt > 15 GeV and |eta| < 3.0 (to have a margin
        // w.r.t. tracker acceptance and treat migrations)
        if (genmu.p4.Pt() < 10 /* GeV */ || abs(genmu.p4.Eta()) > 3.0) continue;

        genMuons_->push_back(genmu);
    }

    Sort<DAS::GenMuon>(genMuons_);
}

namespace /* anonymous */ {
/// Helper function to check if a gen particle has a Z boson in its ancestors
bool hasZAncestor (const reco::Candidate &gp) {
    return gp.pdgId() == 23 || (gp.numberOfMothers() > 0 && hasZAncestor(*gp.mother(0)));
}
} // anonymous namespace

////////////////////////////////////////////////////////////////////////////////
/// Finds the generated photons and applies some hard cuts to the phase space
void Ntupliser::getGenPhotons ()
{
    // Collect charged leptons that would be dressed
    vector<const reco::GenParticle *> charged_leptons;
    for (const reco::GenParticle &gp: *genParticles) {
        auto id = abs(gp.pdgId());
        if (gp.status() == 1 && (id == 11 || id == 13))
            charged_leptons.push_back(&gp);
    }

    for (const reco::GenParticle &gp: *genParticles) {
        // Consider stable (status 1) photons
        if (gp.status() != 1 || gp.pdgId() != 22) continue;

        // Phase space cuts
        const bool zAncestor = hasZAncestor(gp);
        if (zAncestor && gp.pt() < 1) continue;
        if (!zAncestor && (gp.pt() < 10 || abs(gp.eta()) > 3.0)) continue;

        // We store dressed leptons. Remove any photon falling within the
        // dressing cones, since we'd have no way of removing them afterwards.
        if (any_of(charged_leptons.begin(), charged_leptons.end(),
                   [gp](auto lep) { return reco::deltaR(lep->p4(), gp.p4()); }))
            continue;

        genPhotons_->push_back(h.GetGenPhoton(gp, zAncestor));
    }
}


////////////////////////////////////////////////////////////////////////////////
/// Finds the reconstructed jets, and applies some hard cuts on the phase space.
/// 
/// `MyJetCollection` is expected to be either `JetCollection` or `JetFlavourInfoMatchingCollection`
/// (according to the flavour flag)
template<typename MyJetCollection> void Ntupliser::getGenJets (edm::Handle<MyJetCollection>& mygenjets)
{
    //for ((typename MyJetCollection)::const_iterator itJet = mygenjets->begin();
    for (auto itJet = mygenjets->begin();
            itJet != mygenjets->end(); ++itJet) {

        // we convert the complicated CMSSW object to our simple DAS object
        DAS::GenJet genjet = h.GetGenJet(*itJet);

        // we keep only jet with pt > 20 GeV and in the tracker acceptance
        //if (genjet.p4.Pt() < 10 /* GeV */ || abs(genjet.p4.Eta()) > 5.0) continue; //TODO externalize option for phase space cut
        // (note: slimmedJets are only defined from 10 GeV)

        genJets_->push_back(genjet);
    }

    Sort<DAS::GenJet>(genJets_);
}

////////////////////////////////////////////////////////////////////////////////
/// Finds the reconstructed muons, and applies some hard cuts on the phase space.
void Ntupliser::getRecMuons ()
{
    for(pat::MuonCollection::const_iterator itMu = recmuons->begin();
            itMu != recmuons->end(); ++itMu) {

        // we convert the complicated CMSSW object to our simple DAS object
        DAS::RecMuon recmu = h.GetRecMu(*itMu);

        // we keep only muons with pt > 10 GeV and in the muon chamber acceptance
        if (recmu.p4.Pt() < 10 /* GeV */ || abs(recmu.p4.Eta()) > 2.4) continue;

        recMuons_->push_back(recmu);
    }

    Sort<DAS::RecMuon>(recMuons_);
}

////////////////////////////////////////////////////////////////////////////////
/// Finds the reconstructed photons, and applies some hard cuts on the phase space.
void Ntupliser::getRecPhotons ()
{
    for (pat::PhotonCollection::const_iterator it = recphotons->begin();
         it != recphotons->end(); ++it) {

        // we convert the complicated CMSSW object to our simple DAS object
        DAS::RecPhoton recphoton = h.GetRecPhoton(*it);

        // we keep only photons with pt > 5 GeV
        if (recphoton.p4.Pt() < 5 /* GeV */) continue;

        recPhotons_->push_back(recphoton);
    }

    Sort<DAS::RecPhoton>(recPhotons_);
}


////////////////////////////////////////////////////////////////////////////////
/// Finds the reconstructed jets, and applies some hard cuts on the phase space.
void Ntupliser::getRecJets ()
{
    for(pat::JetCollection::const_iterator itJet = recjets->begin();
            itJet != recjets->end(); ++itJet) {

        // we convert the complicated CMSSW object to our simple DAS object
        DAS::RecJet recjet = h.GetRecJet(*itJet);
        
        // we keep all jets and give set the weight to 0 for jets not fullfiling the quality criterion 
        if (!h.TightLepVetoID(*itJet)) recjet.weights.front() = 0.;

        // we keep only jet with pt > 20 GeV and in the tracker acceptance
        //if (recjet.p4.Pt() < 10 /* GeV */ || abs(recjet.p4.Eta()) > 5.0) continue; //TODO externalize option for phase space cut
        // (note: slimmedJets are only defined from 10 GeV)

        recJets_->push_back(recjet);
    }

    Sort<DAS::RecJet>(recJets_);
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Called before looping over the events.
///
/// Basically, it set up the branches.
void Ntupliser::beginJob() 
{
    cout << __FILE__ << ':' << __func__ << endl;

    //--- book the tree ----------------------------------
    tree = fs_->make<TTree>("inclusive_jets","inclusive_jets");
    //DT::MetaInfo metainfo(tree, config /* TODO */); // TODO: make sure that this may be run on CRAB...

    // event
    tree->Branch("event",&event_);
    if (p.triggers) {
        tree->Branch("jetTrigger",&jetTrigger_);
        tree->Branch("muonTrigger",&muonTrigger_);
    }

    tree->Branch("pileup",&pileup_);
    tree->Branch("primaryvertex",&primaryvertex_);
    if (p.isMC_)
        tree->Branch("distGenPV", &distGenPV);
    tree->Branch("met",&met_);

    // jets
    tree->Branch("recJets",&recJets_);
    if (!p.isMC_) 
        tree->Branch("hltJets", &HLTjets_);
    if (p.isMC_)
        tree->Branch("genJets", &genJets_);

    // muons
    if (p.muons) {
        tree->Branch("recMuons",&recMuons_);
        if (p.isMC_)
            tree->Branch("genMuons", &genMuons_);
    }

    // photons
    if (p.photons) {
        tree->Branch("recPhotons", &recPhotons_);
        if (p.isMC_)
            tree->Branch("genPhotons", &genPhotons_);
    }
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
void Ntupliser::endJob() 
{  
    cout << __FILE__ << ':' << __func__ << endl;
}
void Ntupliser::beginRun(Run const& iRun, EventSetup const& iSetup) 
{
    cout << __FILE__ << ':' << __func__ << endl;
}
void Ntupliser::endRun(Run const& iRun, EventSetup const& iSetup) 
{
    cout << __FILE__ << ':' << __func__ << endl;
}
#endif

void Ntupliser::analyze(Event const& iEvent, EventSetup const& iSetup) 
{
    // reset and initialise member variables (filled to the tree)
    reset();
    initialise(iEvent);

    // check trigger
    if (p.triggers) {
        bool passTrigger = trigger(iEvent); // TODO: find "skip event" method
        if (!passTrigger) return;
        getHLTjets(iEvent); //Fill High-Level-Trigger jets
    }
    fillMET(iEvent);

    // event variables
    getEventVariables(iEvent);

    // jet variables
    getRecJets();
    if (p.isMC_) {
        if (p.flavour)
            getGenJets(theJetFlavourInfos);
        else
            getGenJets(genjets);
    }

    // muon variables
    if (p.muons) {
        getRecMuons();
        if (p.isMC_)
            getGenMuons();
    }

    // photon variables
    if (p.photons) {
        getRecPhotons();
        if (p.isMC_)
            getGenPhotons();
    }

    tree->Fill();
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Just a common method to reset all branches at each new event.
void Ntupliser::reset()
{
    // event
    event_->clear();
    if (p.triggers) {
        jetTrigger_->clear();
        muonTrigger_->clear();
    }
    pileup_->clear();
    if (p.isMC_)
        distGenPV = -1; // TODO: put this variable in `PrimaryVertex`
    primaryvertex_->clear();
    met_->clear();

    if (p.isMC_) 
        genJets_->clear();
    if (!p.isMC_)
        HLTjets_->clear();
    recJets_->clear();

    if (p.muons) {
        recMuons_->clear();
        if (p.isMC_)
            genMuons_->clear();
    }

    if (p.photons) {
        recPhotons_->clear();
        if (genPhotons_)
            genPhotons_->clear();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Called at the beginning of each new event.
void Ntupliser::initialise (Event const& iEvent)
{
    // jets
    if (p.isMC_) {
#ifdef PS_WEIGHTS
        iEvent.getByToken(p.lheToken);
#endif
        iEvent.getByToken(p.genEvtInfoToken, genEvtInfo);
        iEvent.getByToken(p.genjetsToken, genjets);
        if (p.flavour)
            iEvent.getByToken(p.jetFlavourInfosToken, theJetFlavourInfos );
        iEvent.getByToken(p.genParticlesToken, genParticles);
    }
    iEvent.getByToken(p.recjetsToken, recjets);

    // muons
    if (p.muons) {
        if (p.isMC_)
            iEvent.getByToken(p.genLeptonsToken, genLeptons);
        iEvent.getByToken(p.recmuonsToken, recmuons);
    }

    // photons
    if (p.photons)
        iEvent.getByToken(p.recphotonsToken, recphotons);

    // pile-up
    if (p.isMC_)
        iEvent.getByToken(p.pileupInfoToken, pileupInfo);
    iEvent.getByToken(p.rhoToken,rho);

    // vertex
    iEvent.getByToken(p.recVtxsToken,recVtxs);  

    // trigger
    if (p.triggers) {
        iEvent.getByToken(p.triggerResultsToken, triggerResults);  
        iEvent.getByToken(p.triggerObjectsToken, triggerObjects);

        // HLT
        iEvent.getByToken(p.triggerPrescalesToken,triggerPrescales); 

        // L1
        iEvent.getByToken(p.triggerPrescalesl1minToken, triggerPrescalesl1min);
        iEvent.getByToken(p.triggerPrescalesl1maxToken, triggerPrescalesl1max);
    }

    // MET
    iEvent.getByToken(p.metToken,met);
    iEvent.getByToken(p.metResultsToken, metResults);
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
string DelLastDigits (string n)
{
    while (isdigit(n.back()))
        n.pop_back();
    return n;
}
#endif

//////////////////////////////////////////////////////////////////////////////////////////
/// Finds the bits corresponding to the HLT jet pt triggers.
bool Ntupliser::trigger (Event const& iEvent)
{
    const TriggerNames &names = iEvent.triggerNames(*triggerResults);  

    // example for Run18A (both low & high pt jets)
    // HLT_PFJet15_v    151 151
    // HLT_PFJet25_v    152 152
    // HLT_PFJet40_v    153 153
    // HLT_PFJet60_v    154 154
    // HLT_PFJet80_v    155 155
    // HLT_PFJet140_v   156 156
    // HLT_PFJet200_v   157 157
    // HLT_PFJet260_v   158 158
    // HLT_PFJet320_v   159 159
    // HLT_PFJet400_v   160 160
    // HLT_PFJet450_v   161 161
    // HLT_PFJet500_v   162 162
    // HLT_PFJet550_v   163 163

    bool passTrigger(false);
    for (unsigned int k = 0; k < p.triggerNames_.size(); ++k) {
        bool bit(false);
        int preHLT(1), preL1min(1), preL1max(1);
        string name;

        for (unsigned int itrig = 0; itrig < triggerResults->size(); ++itrig) {
            name = names.triggerName(itrig);

            //--- erase the the version number----
            name = DelLastDigits(name);
            if (name != p.triggerNames_[k]) continue;

            bit = triggerResults->accept(itrig);
            preHLT = triggerPrescales->getPrescaleForIndex(itrig);
            preL1min = triggerPrescalesl1min->getPrescaleForIndex(itrig);
            preL1max = triggerPrescalesl1max->getPrescaleForIndex(itrig);
            break;
        }
        //--- if at least one monitored trigger has fired passTrigger becomes true
        passTrigger += bit;

        if (name.find("HLT_PFJet") != string::npos || name.find("HLT_AK8PFJet") != string::npos) {
            jetTrigger_->Bit.push_back(bit); 
            jetTrigger_->PreHLT.push_back(preHLT);
            jetTrigger_->PreL1min.push_back(preL1min);
            jetTrigger_->PreL1max.push_back(preL1max);
        }
        else if (name.find("HLT_Mu") != string::npos || name.find("HLT_IsoMu") != string::npos) {
            muonTrigger_->Bit.push_back(bit); 
            muonTrigger_->PreHLT.push_back(preHLT);
            muonTrigger_->PreL1min.push_back(preL1min);
            muonTrigger_->PreL1max.push_back(preL1max);
        }
        else throw cms::Exception("Ntupliser") << "No dedicated collection exists for " << name;
    }
    return passTrigger;
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Fill the MET flags to the array
void Ntupliser::fillMET (Event const& iEvent)
{
    const edm::TriggerNames &namesMet = iEvent.triggerNames(*metResults);
    for(unsigned int k=0; k < p.metNames_.size(); ++k) {
        bool bit(false);
        for(unsigned int itrig=0; itrig<metResults->size(); ++itrig) {
            string met_name = string(namesMet.triggerName(itrig));
            if (met_name == p.metNames_[k]) {
                bit = metResults->accept(itrig);
                break;
            }
        }
        met_->Bit.push_back(bit);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Finds HLT jets and simply stores them in a `FourVector`.
void Ntupliser::getHLTjets (Event const& iEvent)
{
    const TriggerNames &names = iEvent.triggerNames(*triggerResults);  

    // loop over all possible triggers and only keep object from the PFJet
    // triggers
    for(TriggerObjectStandAlone obj: *triggerObjects){
        obj.unpackPathNames(names);
        //vector<string> pathNamesAll  = obj.pathNames(false); 
        vector<string> pathNamesLast = obj.pathNames(true);

        // first, look if the HLT object is a HLT jet
        bool isHLTjet = false;
        string sNow;
        for (auto s: pathNamesLast) {
            s = DelLastDigits(s); // remove the version of the trigger (i.e. HLT_PFJetXX_v*)
            if (p.HLTjet_triggerNames.count(s) == 0) continue; // typically ignore mu triggers
            isHLTjet = true;
            sNow = s;
            break;
        }

        // then add it unless it has already been added
        if(isHLTjet) {
            DAS::FourVector P4(obj.pt(), obj.eta(), obj.phi(), obj.mass());
            bool isIn = false;
            for(const auto &v : *HLTjets_)
                if(v == P4) {isIn = true; break;}
            if(!isIn)
                HLTjets_->push_back(P4);
        }
    }

    Sort<DAS::FourVector>(HLTjets_);
}

/////////////////////////////////////////////////////////////////////////////////
/// Gets information about the event (run number, etc.), the pile-up, the MET and
/// the primary vertex.
///
/// https://cmsdoxygen.web.cern.ch/cmsdoxygen/CMSSW_10_6_19/doc/html/d9/d53/classPileupSummaryInfo.html
void Ntupliser::getEventVariables (Event const& iEvent)
{
    // event
    event_->runNo = iEvent.id().run(); // note: always 1 for MC
    event_->evtNo = iEvent.id().event();
    event_->lumi = iEvent.id().luminosityBlock();

    event_->recWgts.resize(1);
    event_->recWgts.front() = 1;

    if (p.isMC_) {
        event_->hard_scale = genEvtInfo->qScale();
#ifdef PS_WEIGHTS
#error "Generator weights haven't been tested yet! Implementation may not be ready"
        // model variations
        auto& weights = lhe->weights();
        for (auto& w: weights) {
            cout << "Weight " << w.id << " " <<  w.wgt << '\n';
            event_->genWgts.push_back(w.wgt); // TODO: multiply by `genEvtInfo->weight()`?
        }
        cout << flush;
#else
        event_->genWgts.resize(1);
        event_->genWgts.front() = genEvtInfo->weight(); 
#endif
    }

    // pile-up
    pileup_->rho  = *rho;
    pileup_->nVtx = recVtxs->size();
    if(p.isMC_) 
    for(auto PUI = pileupInfo->begin(); PUI != pileupInfo->end(); ++PUI) {
        if (PUI->getBunchCrossing() != 0) continue;
        // Adapted from [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData):
        // In order to do reasonable comparisons between data and MC, it is necessary to understand exactly what the histogram produced by pileupCalc.py means so that it can be compared with the correct quantity in MC.
        // The way that pileup events are generated in MC, given an input pileup distribution, is as follows:
        pileup_->trpu = PUI->getTrueNumInteractions(); // average pileup conditions under which the event is generated
        pileup_->intpu = PUI->getPU_NumInteractions(); // the number of pileup events for the in-time bunch crossing is selected from a Poisson distribution with a mean equal to the "true" pileup

        auto &ptHatVec = PUI->getPU_pT_hats();
        pileup_->pthatMax = ptHatVec.size() > 0 ? *max_element(ptHatVec.begin(), ptHatVec.end()) : 0;
    }

    // primary vertex
    const auto& PV = (*recVtxs)[0];
    primaryvertex_->Rho  = PV.position().Rho();
    primaryvertex_->z    = PV.z();
    primaryvertex_->ndof     = PV.ndof();
    primaryvertex_->chi2     = PV.chi2();
    primaryvertex_->fake     = PV.isFake();
    if (p.isMC_) {
        const auto& rv = PV.position();
        const auto& gv = (*genParticles)[0].daughter(0)->vertex();
        distGenPV = hypot( hypot(rv.x() - gv.x(),
                                 rv.y() - gv.y()),
                                 rv.z() - gv.z()  ); // TODO: move this in `PrimaryVertex`
    }

    // MET
    met_->Et = (*met)[0].et();
    met_->SumEt = (*met)[0].sumEt();
    met_->Pt = (*met)[0].pt(); // TODO: compare to `(*met)[0].uncorPt()`
    met_->Phi = (*met)[0].phi();
    // https://twiki.cern.ch/twiki/bin/viewauth/CMS/MissingETRun2Corrections#Type_I_Correction_Propagation_of
    // https://twiki.cern.ch/twiki/bin/view/CMS/MissingETUncertaintyPrescription
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
Ntupliser::~Ntupliser() 
{
}
#endif

DEFINE_FWK_MODULE(Ntupliser);
