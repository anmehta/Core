#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <algorithm>
#include <filesystem>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Math/VectorUtil.h"

#include "Core/JetVetoMaps/interface/Conservative.h"

#include <protodarwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetVeto {

////////////////////////////////////////////////////////////////////////////////
/// Remove regions that are not properly simulated both in data and simulation
/// (fixed via unfolding at the end).
///
/// Maps are derived by Hannu (Helsinki), and should be downloaded and adapted.
void applyConservativeVetoMap
            (const vector<fs::path> inputs, //!< input ROOT files (n-tuples)
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");

    const auto& jetveto_file = config.get<fs::path>("corrections.jetvetomap.filename");
    JetVeto::Conservative jetveto(jetveto_file);
    metainfo.Set<fs::path>("corrections", "jetvetomap", "filename", jetveto_file);

    Event * event = nullptr;
    tIn->SetBranchAddress("event", &event);
    vector<RecJet> * recJets = nullptr;
    tIn->SetBranchAddress("recJets", &recJets);

    ControlPlots raw("raw");
    ControlPlots nominal("HotKillers");

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        auto ev_w = (isMC ? event->genWgts.front() : 1) * event->recWgts.front();

        raw(*recJets, ev_w);

        for (auto& recJet: *recJets)
            jetveto(recJet);

        nominal(*recJets, ev_w);
        
        if ((steering & DT::fill) == DT::fill) tOut->Fill();
    }

    fOut->cd();
    jetveto.Write(fOut.get());
    raw.Write(fOut.get());
    nominal.Write(fOut.get());

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::JetVeto namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Apply jet veto maps by setting the weight of reconstructed jet "
                            "to zero whenever that jet is found to be in a veto zone.",
                            DT::config | DT::split | DT::fill);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("jetvetomap", "corrections.jetvetomap.filename", "ROOT file containing jet vet maps");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetVeto::applyConservativeVetoMap(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
