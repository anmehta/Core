#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/toolbox.h"

#include <TFile.h>
#include <TChain.h>
#include <TH2.h>
#include <TString.h>

#include "Math/VectorUtil.h"

#include "Core/Normalisation/interface/TriggerEff.h"
#include "Core/Normalisation/interface/TriggerLumi.h"
#include "Core/Normalisation/interface/PhaseSelection.h"

namespace fs = std::filesystem;

namespace DAS::Normalisation {

////////////////////////////////////////////////////////////////////////////////
/// This functor implements the selection and normalisation of the events in the dataset.
/// The public attributes are the following:
///  - trigger_lumi -> contains the trigger threshold (integer) and associated lumi
///  - eff -> contains the efficiency of the trigger at the leadingJet
///  - ntriggers -> number of trigger used
///  - inv_total_lumi -> inverse of the total uminosity of the sample
///  - minpt -> pt turnon of the lowest trigger
///  - ibit -> index of the highest pt trigger that have fired
///  - it -> iterator to the `pair<trigger, TriggerLumi>` of the highest pt trigger that have fired
///
/// The operator() implements the selection and normalisation itself.  A private 
/// funtion pointer is used to allow to point to different private methods
/// corresponding to different strategies. This behaviour is switched via
/// 'strategy' argument in the constructor. It also iterate `Functor::ibit`
/// down to the highest trigger for which the leading jet pt is above the trigger 
/// turnon. This functor returns the leading jet according to the strategy that 
/// has been chosen AND multiplies the rec-level event weight to normalise the
/// triggers together and to normalise the full dataset to the correct luminosity,
/// according to the method that has been used. The weights are multiplied by:
/// $\frac{ prescale * \mathcurl{L}_{total}^{-1} }{ \epsilon_{trigger} }$ in method 'presc'
/// and by $ \frac{ \mathcurl{L}_{effective, trigger}^{-1} }{ \epsilon_{trigger} }$ in method 'lumi'.
/// (NOTE: The lumi, trigger thresholds and trigger efficiency curves should be provided)
///
/// Two strategies are implemented.
/// - 'pt' (see the private method `Functor::NormalisationStd()`)
/// - 'eta' (see the private method `Functor::NormalisationFwd()`)
/// (NOTE: Other strategies could be implemented in the future. They should be implemented in this class.)
///
/// Two methods are implemented:
///  - 'prescales' (see the private method `Functor::NormalisationCommonPresc()`)
///  - 'lumi' (see the private method `DataNormalsiation::NormalisationCommonLumi()`)
/// (NOTE: Other methods could be implemented in the future. They should be implemented in this class.)
///
/// Technical remark: the switch wrt the different strategies and is done through private 
/// methods and private pointer function.
struct Functor {
    const std::map<int, TriggerLumi> triggers_lumi;
    TriggerEff eff;
    const size_t ntriggers;
    const float inv_total_lumi;
    const float minpt;
    size_t ibit;
    std::map<int, TriggerLumi>::const_reverse_iterator it;

private:
    ////////////////////////////////////////////////////////////////////////////////
    /// Pointer functions. `NormalisationPtr` is set by the constructor to one of private method implementing a specific
    /// normalisation strategy (i.e. normalisation,selection of the leading jet, trigger considered as
    /// triggering the event).
    /// The `NormaloisationCommonPtr` pointer implements the part common to
    /// all the strategies but implementing different method to normalise, depending on the argument `method` in the constructor.
    /// It is used by the private methods pointed by `NormalisationPtr`.
    const RecJet& (Functor::* NormalisationPtr)(Event&, std::vector<RecJet>&, std::vector<FourVector>&, Trigger&);
    const RecJet& (Functor::* NormalisationCommonPtr)(Event&, const RecJet&, const Trigger&);

public:
    ////////////////////////////////////////////////////////////////////////////////
    /// TODO
    Functor (fs::path lumi_file,        // !< path to the a two column file containg trigger lumis
             fs::path turnon_file,      // !< file containing turnons output of `getTriggerTurnons`
             fs::path trigger_curves,   // !< .root trigger curves, output of `getTriggerTurnons`
             const std::string& strategy,    // !< strategy 'pt' or 'eta'
             const std::string& method,      // !< method 'presc' or 'lumi'
             const int year) :          // !< year of the dataset (201?)
        triggers_lumi(GetLumiFromFiles(lumi_file, turnon_file)),
        eff(trigger_curves, triggers_lumi, year),
        ntriggers(triggers_lumi.size()),
        inv_total_lumi((prev(triggers_lumi.end())->second).weight),
        minpt(triggers_lumi.begin()->second.turnon)
    {
        using namespace std;

        cout << "ntriggers = " << ntriggers << '\n'
             << "tot lumi = " << 1./inv_total_lumi << '\n'
             << "minpt = " << minpt << endl;

        if (method == "prescales")
            NormalisationCommonPtr = &Functor::NormalisationCommonPresc;
        else if (method == "lumi")
            NormalisationCommonPtr = &Functor::NormalisationCommonLumi;
        else {
            cerr << "method ('" << method << "') is not defined, aborting\n";
            exit(EXIT_FAILURE);
        }

        if (strategy == "pt")
            NormalisationPtr = &Functor::NormalisationStd;
        else if (strategy == "eta")
            NormalisationPtr = &Functor::NormalisationFwd;
        else {
            cerr << "strategy of selection and normalisation ('" << strategy << "') is not defined, aborting\n";
            exit(EXIT_FAILURE);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Implements the selection and normalisation. It sets the `Event::recWgts` so that the different triggers
    /// are normalised and that the dataset in normalised to the total luminosity. In this respect it is assumed
    /// that the highest pt trigger has prescale 1. And that its lumi is the total luminosity.
    ///
    /// Apply some preliminary cuts depending depending on triggers.
    /// (For more details see the strategies)
    const RecJet& operator() (Event & evnt,
                              std::vector<RecJet>& recJets,
                              std::vector<FourVector>& hltJets,
                              Trigger& trigger)
    {
        return (this->*NormalisationPtr)(evnt, recJets, hltJets, trigger);
    }

private:
    ////////////////////////////////////////////////////////////////////////////////
    /// Implements the part that is common to all the strategies in the 'presc' method.
    ///  - calculating the prescales
    ///  - set the weight according to the prescales, efficiency andi total luminosity
    ///  - remove events with null trigger efficiency
    const RecJet& NormalisationCommonPresc (Event & evnt,
                                            const RecJet& leadingJet,
                                            const Trigger& trigger)
    {
        using namespace std;

        // get prescale
        float preHLT   = trigger.PreHLT[ibit];
        float preL1min = trigger.PreL1min[ibit];
        float preL1max = trigger.PreL1max[ibit];
        if (preL1min != preL1max)
            cerr << "\x1B[31m\e[1m" << preL1min << ' ' <<  preL1max << "\x1B[30m\e[0m\n";

        float prescale = preHLT * preL1min; // TODO: this should be changed
                                             // there are cases in which preL1min != preL1max
        // sanity checks:
        // 1) we assume that the same prescale is indeed constant for a given LS
        {
            static vector<map<pair<int,int>,int>> prescales(ntriggers);
            pair<int,int> runlum = {evnt.runNo, evnt.lumi};
            if (prescales.at(ibit).count(runlum))
                assert(prescales.at(ibit).at(runlum) == prescale);
            else
                prescales.at(ibit)[runlum] = prescale;
        }

        // setting the inverse of the eff lumi to the event including correction of for the trigger efficiency
        float efficiency = eff(leadingJet);
        if (efficiency <= 0) {
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return leadingJet;
        }
        float norm = prescale * inv_total_lumi / efficiency;
        for (size_t i = 0; i < evnt.recWgts.size(); ++i)
            evnt.recWgts.at(i) *= norm;
        return leadingJet;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Implements the common part to all the strategies in the 'lumi' method:
    ///  - find the luminosity that apply to the event
    ///  - set the weight accroding to the effective luminosity and efficiency of the trigger
    ///  - remove events with null trigger efficiency
    /// (NOTE: The method lumi could be used to force the application of a weight.
    /// For instance, inv_effective_prescales * total_inv_lumi)
    const RecJet& NormalisationCommonLumi (Event& evnt,
                                           const RecJet& leadingJet,
                                           const Trigger& trigger)
    {
        // retrieve the efficiency and the effective luminosity
        float efficiency = eff(leadingJet);
        float inv_eff_lumi = (it->second).weight;

        if (efficiency <=0) {
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return leadingJet;
        }
        float norm = inv_eff_lumi / efficiency;
        for (size_t i = 0 ; i < evnt.recWgts.size() ; ++i)
            evnt.recWgts.at(i) *= norm;
        return leadingJet;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Implements the "standard" normalisation. The jets are ordered in pt.
    /// The jets with highest pt within ($|\eta| < 2.5$) is the "leading jet".
    /// Find the trigger bit corresponding to:
    ///     $ max \left( \left{ TriggerThresods | TriggerThresholds < p_{T}^{leading} \right} \right) $
    /// Calculates the efficiency and prescales and reweight the events.
    /// (NOTE: this done regardless to the fact that the `leadingJet` has matched to an `hltJet`)
    const RecJet& NormalisationStd (Event & evnt,
                                    std::vector<RecJet>& recJets,
                                    std::vector<FourVector>& hltJets,
                                    Trigger& trigger)
    {
        using namespace std;

        // we find the leading jet in tracker acceptance
        auto leadingInTk = phaseSel(recJets);

        // remove empty events
        if (leadingInTk == recJets.end()) {
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return recJets.front();
        }
        // we assume that JES corrections have been stored in DAS::RecJet::JECs
        float leading_pt = leadingInTk->CorrPt();
        // remove events if it does not contain any jets with pt > first threshold
        if (leading_pt < minpt) {
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return recJets.front();
        }
        // note: we want to keep jets with pt < minpt if at least the leading jets has > 64 
        // (but other jets are allowed to have a arbitrarily low pt)

        it = triggers_lumi.rbegin();
        ibit = ntriggers-1;
        // find the range by looping over triggers (from top to bottom)
        while (it != triggers_lumi.rend()) {
            if (leading_pt >= it->second.turnon) break;
            ++it; --ibit;
        }

        // if the leading pt was "too low"
        // (i.e. smaller than the lowest-pt turn-on),
        // then the event is discarded

        if (it == triggers_lumi.rend()) {
            cerr << "\x1B[31m\e[1mProblem with `trigger_lumi` iterator (leading_pt = " << leading_pt << ")\x1B[30m\e[0m\n";
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return recJets.front();
        }
        if (ibit >= ntriggers+1) {
            cerr << "\x1B[31m\e[1mProblem with `ibit` (leading_pt = " << leading_pt << ")\x1B[30m\e[0m\n";
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return recJets.front();
        }

        // check if the trigger has fired
        if (!trigger.Bit.at(ibit)) {
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return recJets.front();
        }
        return (this->*NormalisationCommonPtr)(evnt, *leadingInTk, trigger);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Implements the normalisation and selection used for Mueller-Navelet jets studies.
    /// Return the leadingJet.
    /// Calculates the luminosity, efficiency, and prescale and set the wieghts accordingly
    /// The selection rules for event are the following:
    ///         -> Must be the most forward or most backward jet
    ///         -> Must match an hltJet
    ///         -> The hltJet and recJet matching together must be above the hlt threshold and turnon
    ///         of the trigger respectivelly.
    ///         -> If both the most bwd and most fwd jets satisfies the above conditions,
    ///         the jets with highest absolute rapidity is defined as the leading jet.
    /// If the event do not macth these criteria, the event weights are set to zero and the leadingJet
    /// is the most forward jet. The event must be discarted when filling a tree.
    const RecJet& NormalisationFwd (Event & evnt,
                                    std::vector<RecJet>& recJets,
                                    std::vector<FourVector>& hltJets,
                                    Trigger & trigger)
    {
        using namespace std;

        // sorte the jets in decreasing order of rapidity
        auto mnorder = [] (const RecJet& recJet1, const RecJet& recJet2) {
            return (recJet1.CorrEta() > recJet2.CorrEta());
        };
        sort(recJets.begin(), recJets.end(), mnorder);

        // take the most forward and most backward jets
        auto leadingJet = recJets.begin();
        auto subleadingJet = prev(recJets.end());
        FourVector leadingHltTk = match(leadingJet->p4, &hltJets);
        FourVector subleadingHltTk = match(subleadingJet->p4, &hltJets);

        if (leadingHltTk == FourVector() && subleadingHltTk == FourVector()) {
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return recJets.front();
        }

        // we assume that JES corrections have been stored in DAS::RecJet::JECs
        float leading_pt = leadingJet->CorrPt();
        float subleading_pt = subleadingJet->CorrPt();
        // remove events if it does not contained any jet with pt > pt of the first threshold
        if (leading_pt < minpt && subleading_pt < minpt) {
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return recJets.front();
        }

        // note: we want to keep jets with pt < minpt if at least the leading jets has > 64
        // (but other jets are allowed to have a arbitrarily low pt)

        float leadingHlt_pt = leadingHltTk.Pt();
        float subleadingHlt_pt = subleadingHltTk.Pt();

        it = triggers_lumi.rbegin();
        ibit = ntriggers-1;

        // Find the range by looping over triggers (from top to bottom)
        while (it != triggers_lumi.rend()) {
            int recturnon = it->second.turnon;
            float hltthreshold = it->first;
            if ( (leading_pt >= recturnon) && (leadingHlt_pt >= hltthreshold)
                    && (subleading_pt >= recturnon) && (subleadingHlt_pt >= hltthreshold) ) {
                if ( std::abs(subleadingJet->CorrEta()) > std::abs(leadingJet->CorrEta()) ) {
                    swap(leadingJet, subleadingJet);
                    swap(leading_pt, subleading_pt);
                    swap(leadingHlt_pt, subleadingHlt_pt);
                }
                break;
            }
            if (leading_pt >= recturnon && leadingHlt_pt >= hltthreshold) break;
            if (subleading_pt >= recturnon && subleadingHlt_pt >= hltthreshold) {
                swap(leadingJet, subleadingJet);
                swap(leading_pt, subleading_pt);
                swap(leadingHlt_pt, subleadingHlt_pt);
                break;
            }
            ++it; --ibit;
        }

        // if the leading pt was "too low"
        // (i.e. smaller than the lowest-pt turn-on),
        // then the event is discarded
        if (it == triggers_lumi.rend()) {
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return recJets.front();
        }

        if (ibit >= ntriggers+1) {
            cerr << "\x1B[31m\e[1mProblem with `ibit` (leading_pt = " << leading_pt << ")\x1B[30m\e[0m\n";
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return recJets.front();
        }

        //check if the trigger has fired
        if (!trigger.Bit.at(ibit)) {
            fill(evnt.recWgts.begin(), evnt.recWgts.end(), 0.);
            return recJets.front();
        }
        return (this->*NormalisationCommonPtr)(evnt, *leadingJet, trigger);
    }
};

} // end of DAS::Normalisation namespace
