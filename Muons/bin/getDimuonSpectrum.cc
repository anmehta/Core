#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Template for function (TODO)
void getDimuonSpectrum 
             (const fs::path& input,  //!< name of input root file 
              const fs::path& output, //!< name of output root file
              /* TODO: add all arguments you may need */
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    assert(fs::exists(input));
    auto source = TFile::Open(input.c_str(), "READ");
    auto tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();
    bool isMC = metainfo.isMC();

    Event * ev = nullptr;
    tree->SetBranchAddress("event", &ev);

    vector<RecMuon> * recMuons = nullptr;
    tree->SetBranchAddress("recMuons", &recMuons);

    if (fs::exists(output))
        cerr << red << output << " will be overwritten!\n" << normal;
    auto file = TFile::Open(output.c_str(), "RECREATE");

    auto h = new TH1F("dimuon", "", 40, 71, 111);

    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        if (recMuons->size() < 2) continue;

        FourVector diMuon = recMuons->at(0).p4 + recMuons->at(1).p4;
        float w = ev->recWgts.front()
                * recMuons->at(0).weights.front()
                * recMuons->at(1).weights.front();
        if (isMC) w *= ev->genWgts.front();
        h->Fill(diMuon.M(), w);
    }
    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "where\tinput = ...\n" // TODO: fill
             << "     \toutput = ...\n" // TODO: fill
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getDimuonSpectrum(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
