#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS {

namespace Trigger {

// TODO: retrieve turn ons automatically from tables and adapt thresholds for each year
static const vector<float> thresholds {40, 60, 80, 140, 200, 260, 320, 400, 450, 500};
static const vector<float> turnOns = {50, 85.8228, 108.377, 171.332, 239.027, 312.536, 378.93, 451.078, 493.462, 14000};

int getTrigIndex (float pt)
{
    for(int i = turnOns.size()-1; i >= 0; --i) {
        float pTlow  =  (i >= 0) ? turnOns[i] : 0;
        float pThigh =  (i < int(turnOns.size())-1) ? turnOns[i+1] : 1e8;
        if(pTlow < pt && pt < pThigh) {
            //cout << pt <<" "<< turnOns[i] << endl;
            return i;
        }
    }
    return -1;
}

TH1 * MakeHist (const char * type, int runNo)
{
    const char * name = Form("%s_%d", type, runNo),
               * title = Form("%s %d", type, runNo),
    return new TH1D(name, title, thresholds.size()-1, -0.5, -1.5+thresholds.size());
}

} // end of Trigger namespace

////////////////////////////////////////////////////////////////////////////////
/// Get trigger curves from the data *n*-tuples
[[ deprecated ]]
void getRunByRunXsec
            (TString input,      //!< name of input root file with *n*-tuple
             TString output,     //!< name of output root file with trigger curves
             int nSplit = 1,     //!< number of jobs/tasks/cores
             int nNow = 0)       //!< index of job/task/core
{
    assert(thresholds.size() == turnOns.size());
    TH1::SetDefaultSumw2();

    TChain * chain = new TChain("inclusive_jets"); 
    chain->Add(input);

    Trigger * trigger = nullptr;
    Event * event = nullptr;
    chain->SetBranchAddress("trigger", &trigger);
    chain->SetBranchAddress("event", &event);

    vector<RecJet> * recjets = nullptr;
    chain->SetBranchAddress("recJets", &recjets);

    TFile *file = TFile::Open(output, "RECREATE");

    map<int, TH1 *> h_efflumi, h_presc;

    Looper looper(__func__, chain, nSplit, nNow);
    while (looper.Next()) {

        // sanity check
        assert(trigger->Bit.size() >= thresholds.size());

        // avoid empty events
        if (recjets->size() == 0) continue;

        RecJet& leading = recjets->front();
        int runNo = event->runNo;

        if (!h_efflumi.count(runNo)) {
            h_efflumi[runNo] = MakeHist("efflumi", runNo);
            h_presc[runNo] = MakeHist("presc", runNo);
        }

        float pt = leading.p4.Pt();
        int itrig = getTrigIndex(pt);
        //cout << itrig << '\t' << pt << endl;

        // continue if not fired
        if (!trigger->Bit[itrig]) continue;

        h_efflumi.at(runNo)->Fill(itrig);

        assert(trigger->PreL1min[itrig] == trigger->PreL1max[itrig]);
        float Presc = trigger->PreHLT[itrig] * trigger->PreL1min[itrig];
        h_presc.at(runNo)->Fill(itrig, Presc);
    }
    file->Write();
    file->Close();
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = root file with trigger curves" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    DAS::getRunByRunXsec(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
