#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/stream.h"
#include "Core/CommonTools/interface/variables.h"
#include "Math/VectorUtil.h"

namespace DAS::Normalisation {

DAS::FourVector match (const DAS::FourVector & jet, const std::vector<DAS::FourVector>* hltJets) {
    for (const auto& hltjet: *hltJets){
        using ROOT::Math::VectorUtil::DeltaR;
        if (DeltaR(hltjet, jet) < 0.3)
            return hltjet;
    }
    return DAS::FourVector();
}

} // end of DAS::Normalisation namespace
