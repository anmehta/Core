#include <cstdlib>
#include <string>
#include <iostream>
#include <filesystem>

#include <TFile.h>
#include <TROOT.h>
#include <TH2.h>
#include <TF1.h>
#include <TMath.h>

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/variables.h"

#include "Core/JEC/interface/JESreader.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

void nominal (JESreader * jecs, float radius)
{
    vector<int> rhos {0,15,20,25,100};
    static double approx = M_PI*pow(radius,2);
    vector<double> areas{approx*0.9, approx, 1.1*approx};
    map<int, map<double, TH2 *>> L1s;
    for (int rho: rhos)
    for (size_t i = 0; i < areas.size(); ++i) {
        double area = areas.at(i);
        TString name = Form("L1rho%darea%d", rho, static_cast<int>(i)),
                title = Form("#rho = %d, A = %.2f", rho, area);
        TH2 * h = new TH2D(name, title, nPtBins, pt_edges.data(), nEtaBins, eta_edges.data());

        for (int i = 0; i <= h->GetNbinsX(); ++i)
        for (int j = 0; j <= h->GetNbinsY(); ++j) {
            double pt = h->GetXaxis()->GetBinCenter(i),
                   eta = h->GetYaxis()->GetBinCenter(j);

            double corr = jecs->GetL1Fast(pt, eta, area, rho);
            h->SetBinContent(i,j,corr);
        }
        h->Write();
        //L1s[rho][area] = h;
    }
}


////////////////////////////////////////////////////////////////////////////////
/// Obtain JES corrections (L1 FastJet)
void getJEScurvesOffset (TString input, TString output1, float radius/*, TString input2*/) 
{
    fs::path p = input.Data();
    cout << p << endl;
    assert(fs::exists(p) && fs::is_directory(p));
    cout << p.stem().c_str() << endl;

    TFile * f = TFile::Open(output1, "RECREATE");

    cout << radius << endl;
    JESreader * jecs = new JESreader(input.Data(), p.stem().c_str(), radius/*, input2.Data()*/);

    cout << "Nominal" << endl;

    TDirectory * nom = f->mkdir("nominal");
    nom->cd();
    nominal(jecs, radius);

    f->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output radius\n"
             << "\twhere\tinput = JetMET tables\n"
             << "\t     \toutput = root files with histograms and fits\n"
             << "\t     \tradius = 0.4 or 0.8\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    float radius = atof(argv[3]);

    getJEScurvesOffset(input, output, radius);
    return EXIT_SUCCESS;
}
#endif

