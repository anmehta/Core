#include <cstdlib>
#include <string>
#include <iostream>
#include <memory>

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TH2.h>
#include <TRegexp.h>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/ControlPlots.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/JEC/interface/Scale.h"

#include <protodarwin.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Apply JES corrections to data and MC *n*tuples, as well as uncertainties
/// in data (all sources are considered separately)
void applyJEScorrections
            (const vector<fs::path> inputs, //!< input ROOT files (n-tuples)
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{                               
    cout << __func__ << ' ' << slice << " start" << endl;

    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    auto R = metainfo.Get<int>("flags", "R");

    Event * evnt = nullptr;
    PileUp * pu = nullptr;
    tIn->SetBranchAddress("event", &evnt);
    tIn->SetBranchAddress("pileup", &pu);

    vector<RecJet> * recJets = nullptr;
    tIn->SetBranchAddress("recJets", &recJets);

    const auto JEStables = config.get<fs::path>("corrections.JEStables"); //!< path to directory containing corrections
    metainfo.Set<fs::path>("corrections", "JEStables", JEStables);

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw");
    vector<ControlPlots> calib { ControlPlots("nominal") };

    bool applySyst = (!isMC) && (steering & DT::syst) == DT::syst;
    if (applySyst)
    for (string source: JetEnergy::Scale::uncs) {
        metainfo.Set<string>("variations", "JECs", source + "dn" );
        metainfo.Set<string>("variations", "JECs", source + "up" );

        calib.push_back(ControlPlots(source + "dn"));
        calib.push_back(ControlPlots(source + "up"));
    }

    JetEnergy::Scale jes(JEStables.c_str(), R);

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        auto weight = evnt->recWgts.front();
        if (evnt->genWgts.size() > 0)
            weight *= evnt->genWgts.front();

        // CP before JES
        raw(*recJets, weight);

        for (auto& recJet: *recJets) {

            // sanity check
            if (recJet.JECs.size() != 1 || recJet.JECs.front() != 1)
                BOOST_THROW_EXCEPTION( DE::AnomalousEvent("Unexpected JES corrections", tIn) );

            if (applySyst)
                recJet.JECs.reserve(JetEnergy::Scale::uncs.size()*2+1);

            // nominal value
            auto corr = jes.getJEC(recJet, pu->rho);
            cout << recJet.p4.Pt() << ' ' << corr << '\n';
            recJet.JECs.front() = corr;

            // if data, load uncertainties in JECs
            if (!applySyst) continue;
            recJet.JECs = jes.getUncVec(recJet.p4.Pt(), recJet.p4.Eta()); // all sources
            for (auto& JEC: recJet.JECs) JEC *= corr;
        }
        cout << flush;

        // CP after corrections
        for (size_t i = 0; i < calib.size(); ++i)
            calib.at(i)(*recJets, weight, i);

        sort(recJets->begin(), recJets->end(), pt_sort); // sort again the jets by pt
        if ((steering & DT::fill) == DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();
    raw.Write(fOut.get());
    for (auto& c: calib)
        c.Write(fOut.get());

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Apply jet energy scale corrections. In practice, the correction factors "
                            "are stored in a dedicated vector in the reconstructed jets.",
                            DT::config | DT::split | DT::fill | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("JEStables", "corrections.JEStables", "directory to tables provided by JetMET "
                                                                    "(in txt format)");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::applyJEScorrections(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
