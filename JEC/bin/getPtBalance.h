#ifndef DAS_PT_BALANCE
#define DAS_PT_BALANCE

#include <vector>
#include <functional>
#include <type_traits>

#include <TH3.h>
#include <TString.h>
#include <TDirectory.h>

#include "Core/CommonTools/interface/variables.h"

#include "Math/VectorUtil.h"
#include "common.h"

namespace DAS::JetEnergy {

template<typename Jet> bool DijetSelection (const std::vector<Jet> * jets)
{
   if (jets->size() < 2) return false;
   using ROOT::Math::VectorUtil::DeltaPhi;
   if (DeltaPhi(jets->at(0).p4, jets->at(1).p4) < 2.7) return false;
   if (std::abs(jets->at(0).p4.Eta()) > 2.5 || std::abs(jets->at(1).p4.Eta()) > 2.5) return false;

   if (jets->size() > 2) {

         if constexpr (std::is_same<Jet,RecJet>()) {
              const auto pt0 = jets->at(0).CorrPt(),
                         pt1 = jets->at(1).CorrPt(),
                         pt2 = jets->at(2).CorrPt();
              if (pt2 > 0.15*(pt0 + pt1)) return false;
         }
         else {
              const auto pt0 = jets->at(0).p4.Pt(),
                         pt1 = jets->at(1).p4.Pt(),
                         pt2 = jets->at(2).p4.Pt();
              if (pt2 > 0.15*(pt0 + pt1)) return false;
         }

   }
   return true;
}

template<typename Jet> struct Plots {
    static const std::vector<double> y_edges;

    TString n;
    std::vector<TH3 *> hs;
    std::function<bool(const Jet&, const Jet&)> condition;

    Plots (const char * name, //!< general name (will be used for the directory)
           int N, //!< number of variations
           std::function<bool(const Jet&, const Jet&)> f) : //!< condition for filling
        n(name),
        hs(N),
        condition(f)
    {
        using namespace std;

        auto balBins =  getBinning(60, -0.6, 0.6);
        assertValidBinning(balBins);
        cout << balBins.size() << endl;

        for(int i = 0; i < N; ++i)
            hs[i] = new TH3D(Form("%s_balance_%d", name, i), Form("%s_balance_%d", name, i), //var[i], // TODO: give proper name to variation
                    balBins.size()-1, balBins.data(), nPtBins, pt_edges.data(), nYbins, y_edges.data());
    }

    void Write (TFile * f)
    {
        f->cd();
        TDirectory * d = f->mkdir(n); // member of the class
        d->cd();
        for(TH3 * h: hs) {
            h->SetDirectory(d);
            TString hname = h->GetName();
            hname.ReplaceAll(n + "_", ""); // n is a member of the class
            h->Write(hname);
        }
    }
};

} // end of DAS::JetEnergy namespace
#endif
