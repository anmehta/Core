#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH3.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Template for function
void getJESetaBins 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    /// Opening source and checking that it is MC
    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();

    Event * ev = nullptr;
    tree->SetBranchAddress("event", &ev);

    vector<RecJet> * recjets = nullptr;
    tree->SetBranchAddress("recJets", &recjets);

    auto file = TFile::Open(output, "RECREATE");

    TH3 * nomBinning     = new TH3F("nomBinning"    , "nominal JES binning;p_{T}   [GeV];y;#eta", nPtBins, pt_edges.data(), nYbins, y_edges.data(),    nEtaBins,     eta_edges.data()),
        * nomBinningCorr = new TH3F("nomBinningCorr", "nominal JES binning;p_{T}   [GeV];y;#eta", nPtBins, pt_edges.data(), nYbins, y_edges.data(),    nEtaBins,     eta_edges.data()),
        * uncBinning     = new TH3F("uncBinning", "uncertainty JES binning;p_{T}   [GeV];y;#eta", nPtBins, pt_edges.data(), nYbins, y_edges.data(), nEtaUncBins, eta_unc_edges.data());

    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        auto evWgt = ev->genWgts.front() * ev->recWgts.front();

        for (auto recjet: *recjets) {
            auto absy = recjet.AbsRap(),
                 jetWgt = recjet.weights.front(),
                 w = evWgt * jetWgt;

            nomBinning->Fill(recjet.RawPt(), absy, recjet.RawEta(), w);
            uncBinning->Fill(recjet.RawPt(), absy, recjet.RawEta(), w);
            nomBinningCorr->Fill(recjet.CorrPt(), absy, recjet.CorrEta(), w);
        }
    }
    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getJESetaBins(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
