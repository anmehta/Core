#include <cstdlib>
#include <string>
#include <iostream>
#include <filesystem>

#include <TFile.h>
#include <TROOT.h>
#include <TH2.h>
#include <TF1.h>
#include <TMath.h>

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/Greta.h"

#include "Core/JEC/interface/JESreader.h"

#include "common.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

static const TString formula = "Greta";
//static const TString formula = "[0]+[1]*log(x)+[2]*(log(x))^2+[3]*(log(x))^3+[4]*(log(x))^4"; 
//static const TString formula = "pol4";
static const int Npar = 5;

////////////////////////////////////////////////////////////////////////////////
/// Assign an artificial statistical uncertainty in order to perform fits,
/// reducing the impact from regions where we do not trust the values
/// directly extracted from JetMET tables
void AssignStatUnc (TH2 * h, float radius)
{
    for (int i = 1; i <= h->GetNbinsX(); ++i)
    for (int j = 1; j <= h->GetNbinsY(); ++j) {
        double pt = h->GetXaxis()->GetBinCenter(i),
               y = h->GetYaxis()->GetBinCenter(j);
        double error = 0.001; // default

        if (pt < 200) error *= 4; // most discontinuous region

        if (radius < 0.6) {
            if (y > -2.04 && y < -1.93 && pt > 400 && pt < 1000) error *= 20;
            if (y > 1.04 && y < 1.13 && pt > 1000) error *= 20;
            if (y > -1.93 && y < -1.83 && pt > 400 && pt < 1327) error *= 20;
        }

        h->SetBinError(i,j,error);
    }
}

//int FindLastBin (TH1 * h)
//{
//    const int N = h->FindBin(6000);
//    double last = h->GetBinContent(N);
//    cout << last;
//    for (int i = N; i > 0; --i) {
//        double content = h->GetBinContent(i-1); // we want to stop before the first bin of change
//        if (abs(content - last) > 0.0001) return i;
//    }
//    return N;
//}
static const vector<double> eta_edgesMaxPts { 206.759,297.327,433.089,425.681,751.756,755.473,756.001,761.252,1018.18,1029.83,1033.65,1038.74,1019.09,984.025,998.363,1915.84,1947.03,1973.61,1996.99,2445.08,2465.22,2504.51,2512.22, 2982.1,2982.16,2993.62,2997.65,2989.83,2998.48,2537.48,2537.48,2531.61,2540.85,2533.24,2529.02,2997.06,2997.81,2519.89,2504.51,2482.13,2464.11,1990.02,1508.37,1952.48,1915.02,1003.95,979.516,1012.48,1037.56,1022.57,1007.93, 1008.4, 768.46,765.256,762.723,756.277,427.799,436.055,295.143,204.423};
//static const int nEtaBinsMaxPts= eta_edgesMaxPts.size()-1;


////////////////////////////////////////////////////////////////////////////////
/// Fit of the nominal values of the JECs/
/// Certain bins exhibiting unphysical deviations are treated different:
/// watch out the code...!
map<double, TString> Fit (TH2 * h, float radius)
{
    map<double, TString> corrections;

    static const int Ny = nAbsEtaBins;

    for (int y = 1; y <= Ny; ++y) {
        // project histograms

        TH1 * hp = h->ProjectionX(Form("h_ybin%dp", y), Ny+  y, Ny+  y),
            * hn = h->ProjectionX(Form("h_ybin%dn", y), Ny+1-y, Ny+1-y);
        hp->Write();
        hn->Write();

        double ymin = h->GetYaxis()->GetBinLowEdge(Ny+y  ),
               ymax = h->GetYaxis()->GetBinLowEdge(Ny+y+1);

        double JECmaxYbinP = eta_edgesMaxPts.at(Ny-1+y),
               JECmaxYbinN = eta_edgesMaxPts.at(Ny-  y);

        cout << y << ' ' << ymin << ' '<< ymax << ' ' << JECmaxYbinP << ' ' << JECmaxYbinN << endl;

        // define functions

        double m = hp->GetBinLowEdge(1);

        TF1 * fp = GetSmoothFit<Npar>(hp, m, JECmaxYbinP, true);
        TF1 * fn = GetSmoothFit<Npar>(hn, m, JECmaxYbinN, true);
        fp->SetName(Form("f_ybin%dp", y)); 
        fn->SetName(Form("f_ybin%dn", y)); 

        // fit and store parameters

        hp->Fit(fp, "NQRS", "", m, JECmaxYbinP);
        fp->Write();
        corrections[ymin] = Form("%.3f\t%.3f\t%.3f\t%.3f", ymin, ymax, m, JECmaxYbinP);
        for (int i = 0; i < Npar; ++i) 
            corrections[ymin] += Form("\t%f", fp->GetParameter(i));

        hn->Fit(fn, "NQRS", "", m, JECmaxYbinN);
        fn->Write();
        corrections[-ymax] = Form("%.3f\t%.3f\t%.3f\t%.3f", -ymax, -ymin, m, JECmaxYbinN);
        for (int i = 0; i < Npar; ++i) 
            corrections[-ymax] += Form("\t%f", fn->GetParameter(i));

        // exceptions

        if (radius < 0.6f) {
            if (abs(ymin - 1.13) < 0.01) { // take the result from y < 0 also for y > 0
                fp = fn;
                corrections[ymin] = Form("%.3f\t%.3f\t%.3f\t%.3f", ymin, ymax, m, JECmaxYbinN);
                for (int i = 0; i < Npar; ++i) 
                    corrections[ymin] += Form("\t%f", fn->GetParameter(i));
            }

            if (abs(-ymax + 1.83) < 0.01) { // take the result from y > 0 also for y < 0
                fn = fp;
                corrections[-ymax] = Form("%.3f\t%.3f\t%.3f\t%.3f", -ymax, -ymin, m, JECmaxYbinP);
                for (int i = 0; i < Npar; ++i) 
                    corrections[-ymax] += Form("\t%f", fp->GetParameter(i));
            }
        }
    }

    return corrections;
}

////////////////////////////////////////////////////////////////////////////////
/// Write smoothed corrections to file
void WriteCorrections (TString name, const map<double, TString>& corrections)
{
    ofstream o(name);
    o << "ymin\tymax\tptmin\tptmax\t" << formula << '\t' << Npar << '\n';
    for (auto corr: corrections) o << corr.second << '\n';
    o.close();
}

TH2 * nominal (JESreader * jecs, bool smooth, float radius)
{
    vector<double> logpt_edges = GetLogBinning(20, 3200, 1000);
    int nlogptBins = logpt_edges.size()-1;
    TH2 * global     = new TH2D("global"    , "global"    , nlogptBins, logpt_edges.data(), nEtaBins, eta_edges.data());
    TH2 * L2         = new TH2D("L2Relative", "L2Relative", nlogptBins, logpt_edges.data(), nEtaBins, eta_edges.data());
    TH2 * L2smoothed = new TH2D("L2Smoothed", "L2Smoothed", nlogptBins, logpt_edges.data(), nEtaBins, eta_edges.data());

    for (int i = 0; i <= global->GetNbinsX(); ++i)
    for (int j = 0; j <= global->GetNbinsY(); ++j) {
        double pt = global->GetXaxis()->GetBinCenter(i),
               eta = global->GetYaxis()->GetBinCenter(j);

        {
            static double area = M_PI*pow(radius,2), rho = 20;
            double corr = jecs->getJEC(pt, eta, area, rho);
            global->SetBinContent(i,j,corr);
        }

        {
            double corr = jecs->GetL2Relative(pt, eta);
            L2->SetBinContent(i,j,corr);
        }

        if (smooth) {
            double corr = jecs->GetSmoothL2Relative(pt, eta);
            L2smoothed->SetBinContent(i,j,corr);
        }
    }
    for (TH2 * h: {global, L2, L2smoothed}) 
        h->Write();
    return L2;
}

void uncertainties (JESreader * jecs)
{
    auto uncNames = JESreader::getJECUncNames();
    int N = uncNames.size();

    vector<TH2 *> uncertainties;
    for (auto uncName: uncNames) {
        // since up and down are symmetric, we only store "up"
        TH2 * h = new TH2D(uncName, uncName + "_up", nPtBins, pt_edges.data(), nEtaUncBins, eta_unc_edges.data());
        uncertainties.push_back(h);
    }

    cout << "Filling histograms with JES variation binning" << endl;

    for (int i = 0; i <= uncertainties.front()->GetNbinsX(); ++i)
    for (int j = 0; j <= uncertainties.front()->GetNbinsY(); ++j) {
        double pt  = uncertainties.front()->GetXaxis()->GetBinCenter(i),
               eta = uncertainties.front()->GetYaxis()->GetBinCenter(j);

        auto corrs = jecs->getUncVec(pt, eta);
        for (int k = 0; k < N; ++k) {
            double corr = corrs.at(2*k+1); // since up and down are symmetric, we only store "up"
            uncertainties[k]->SetBinContent(i,j,corr);
        }
    }
    for (TH2 * h: uncertainties) 
        h->Write();
}

////////////////////////////////////////////////////////////////////////////////
/// Obtain JES corrections and uncertainties (L2 MCtruth) + smooth versions
void getJEScurvesMCtruth (TString input, TString output1, TString output2, float radius, TString input2) 
{
    fs::path p = input.Data();
    cout << p << endl;
    assert(fs::is_directory(p));
    cout << p.stem().c_str() << endl;

    TFile * f = TFile::Open(output1, "RECREATE");

    cout << radius << endl;
    JESreader * jecs = new JESreader(input.Data(), p.stem().c_str(), radius, input2.Data());

    cout << "Nominal" << endl;

    TDirectory * nom = f->mkdir("nominal");
    nom->cd();
    bool smooth = fs::exists(fs::path(input2.Data()));
    TH2 * L2 = nominal(jecs, smooth, radius);
    L2->SetDirectory(0);
    f->cd();

    cout << "Uncertainties" << endl;

    TDirectory * unc = f->mkdir("uncertainties");
    unc->cd();
    uncertainties(jecs);
    f->cd();

    cout << "Fitting" << endl;

    AssignStatUnc(L2, radius);

    TDirectory * fits = f->mkdir("fits");
    fits->cd();
    auto corrections = Fit(L2, radius);
    WriteCorrections(output2, corrections);
    f->cd();

    f->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 5) {
        cout << argv[0] << " input output1 output2 radius [input2]\n"
             << "\twhere\tinput = JetMET tables\n"
             << "\t     \toutput1 = root files with histograms and fits\n"
             << "\t     \toutput2 = txt file with fit parameters in similar format as JetMET tables\n"
             << "\t     \tradius = 0.4 or 0.8\n"
             << "\t     \tinput2 = former run of smooth corrections in txt format (same format as output2)" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output1 = argv[2],
            output2 = argv[3];

    float radius = atof(argv[4]);

    TString input2 = argc > 5 ? argv[5] : "";

    getJEScurvesMCtruth(input, output1, output2, radius, input2);
    return EXIT_SUCCESS;
}
#endif
